ARG BUILD_IMAGE="artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.12.0"
ARG BASE_IMAGE="artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.12.0"

FROM $BUILD_IMAGE AS buildenv
FROM $BASE_IMAGE

ENV APP_USER="senscalc"
ENV APP_DIR="/app"

ARG CAR_PYPI_REPOSITORY_URL=https://artefact.skao.int/repository/pypi-internal
ENV PIP_INDEX_URL ${CAR_PYPI_REPOSITORY_URL}/simple

USER root

RUN adduser $APP_USER --disabled-password --home $APP_DIR

WORKDIR $APP_DIR

COPY --chown=$APP_USER:$APP_USER . .

RUN poetry config virtualenvs.create false
# Install runtime dependencies and the app
RUN poetry install

USER ${APP_USER}

CMD ["python3"]