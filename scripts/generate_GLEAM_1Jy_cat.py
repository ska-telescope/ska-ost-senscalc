"""
Script to extract bright sources from the GLEAM ExtraGalactic Catalogue (EGC).
The EGC catalogue can be downloaded from vizier using this link:
https://cdsarc.cds.unistra.fr/viz-bin/cat/VIII/100#/browse
You will need the file GLEAM_EGC_v2.fits

The FITS file has two HDUs: PrimaryHDU and TableHDU. TableHDU contains 307455
sources. Each row has 311 fields. We are only interested in the following
fields (index is 0-based):
3 - RA in J2000 HMS format
4 - Dec in J2000 DMS format
156 - Integrated flux in the 147-154MHz image

The script works on the GLEAM catalogue from Hurley-Walker et al. (2020) and
identifies sources brighter than 1 Jy at 150 MHz.
"""
import numpy as np
from astropy.io import fits

# Read the FITS file and get access to the EGC catalog
file_name = "GLEAM_EGC_v2.fits"
egc_table = fits.open(file_name)[1].data
n_selected_sources = 0
selected_sources = []
for source in egc_table:
    if source[156] >= 1.0:
        n_selected_sources += 1
        selected_sources.append(
            np.asarray([source[3], source[4], source[156]])
        )
# Nine bright sources were peeled from the GLEAM data. They
# are not part of the EGC v2 catalogue. Add these sources
# manually. See Table 2 in Hurley-Walker et al. (2017).
# 3C 444
selected_sources.append(["22:14:26", "-17:01:36", f"{60*(150/200)**-0.96}"])
# Cen A
selected_sources.append(["13:25:28", "-43:01:09", f"{1370*(150/200)**-0.50}"])
# Hydra A
selected_sources.append(["09:18:06", "-12:05:44", f"{280*(150/200)**-0.96}"])
# Pictor A
selected_sources.append(["05:19:50", "-45:46:44", f"{390*(150/200)**-0.99}"])
# Her A
selected_sources.append(["16:51:08", "04:59:33", f"{377*(150/200)**-1.07}"])
# Vir A
selected_sources.append(["12:30:49", "12:23:28", f"{861*(150/200)**-0.86}"])
# Crab
selected_sources.append(["05:34:32", "22:00:52", f"{1340*(150/200)**-0.22}"])
# Cyg A
selected_sources.append(["19:59:28", "40:44:02", f"{7920*(150/200)**-0.78}"])
# Cas A
selected_sources.append(["23:23:28", "58:48:42", f"{11900*(150/200)**-0.41}"])

print(f"Found {n_selected_sources} sources brighter than 1 Jy")

# Write out the selected >1 Jy source as a npy file
selected_sources = np.asarray(selected_sources)
np.save("GLEAM_1Jy_cat.npy", selected_sources)
