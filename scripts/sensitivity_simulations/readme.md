# Simulatons to estimate the relative sensitivty and beam shape

Installation of RASCIL:
```bash
pyenv install anaconda3-2021.05
pyenv virtualenv anaconda3-2021.05 sp-1642
pyenv shell sp-1642
conda install -c conda-forge python-casacore=3.3.1 
pip install -r requirements.txt
# From the RASCIL directory
pip install -e .
# or
pip install .
```

set up commands (example)
```bash
export RASCIL=/Users/nrg47758/work/projects/SKA/ska_tickets/at2-840/rascil
pyenv activate sp-1642
pyenv shell sp-1642
```

Run without modification:
```bash
python $RASCIL/rascil/apps/sensitivity.py \
    --imaging_cellsize 2e-7 \
    --imaging_npixel 1024 \
    --imaging_weighting uniform \
    --rmax 1e5 \
    --imaging_taper 6e-7
```