"""
This script generates the file confusion_noise.ecsv - the lookup table
used to calculate the confusion noise for a given beam size and frequency.
"""
#!/usr/bin/env python

import numpy as np
import astropy.units as u
from scipy import fftpack
from astropy.table import QTable
from pathlib import Path
#
# # Output directory
output_file = Path(__file__).resolve().parents[1] / "src/ska_ost_senscalc/static/lookups/confusion_noise.ecsv"

poly = np.polynomial.Polynomial


# Define source count model at 1.4 GHz
def calc_counts1400():

    # 7th order polynomial fit to MeerKAT DEEP2 and NVSS counts from Matthews+2021, valid between 20 uJy - 25 Jy
    # y = a + b*x + c*x**2 + d*x**3 +e*x**4 + f*x**5 + g*x**6 + h*x**7
    # where y = log10(dN/dS S^2) and x = log10(S)

    # 7th order polynomial fit to 1.4 GHz model counts from Bonaldi+2019, valid between 10 nJy - 20 uJy
    # y = a2 + b2*x + c2*x**2 + d2*x**3 +e2*x**4 + f2*x**5 + g2*x**6 + h2*x**7

    # Set flux boundaries
    s1 = np.log10(2.5e1)
    s2 = -5.0
    s3 = -9.9

    # Parameters of polynomial fit to Matthews+2021 counts

    coef = [2.4927e00, -6.4590e-01, -2.1586e-01, 7.1599e-02, -1.7189e-02, -2.7833e-02, -5.9706e-03, -3.6244e-04]

    # Parameters of polynomial fit to Bonaldi+2019 counts
    coef2 = [4.4744e00, 2.2529e00, 6.3076e-01, -4.1357e-02, -4.0847e-02, -6.1770e-03, -3.7928e-04, -8.3984e-06]

    # Calculate counts
    s = np.linspace(s3, s1, 1000)
    ds = 10 ** s[1:] - 10 ** s[:-1]
    s = (s[1:] + s[:-1]) / 2
    count_brwgt = 10 ** np.concatenate(
        (poly(coef2)(s[s < s2]), poly(coef)(s[s >= s2]))
    )
    s = 10**s
    count = count_brwgt / s**2

    return s, count, ds


# Define source count model at 150 MHz
def calc_counts150():

    # 7th order polynomial fit to LoTSS Deep Fields & TGSS-ADR1 counts from Mandal+2021, valid between 0.2 mJy - 10 Jy
    # y = a + b*x' + c*x'**2 + d*x'**3 +e*x'**4 + f*x'**5 + g*x'**6 + h*x'**7
    # where y = log10(dN/dS S^2.5) and x' = log10(S)+3

    # 7th order polynomial fit to 150 MHz model counts from Bonaldi+2019, valid between 80 nJy - 0.2 mJy
    # y = a2 + b2*x + c2*x**2 + d2*x**3 +e2*x**4 + f2*x**5 + g2*x**6 + h2*x**7
    # where x = log10(S)

    # Set flux boundaries
    s1 = np.log10(1.0e1)
    s2 = np.log10(2.0e-4)
    s3 = -9.1

    # Parameters of polynomial fit to Mandal+2021 counts
    coef = [1.655, -0.1150, 0.2272, 0.51788, -0.449661, 0.160265, -0.028541, 0.002041]

    # Parameters of polynomial fit to Bonaldi+2019 counts
    coef2 = [1.2206e01, 1.6792e01, 1.1309e01, 4.0628e00, 8.3918e-01, 1.0101e-01, 6.5600e-03, 1.7698e-04]

    # Calculate counts
    s = np.linspace(s3, s1, 1000)
    ds = 10 ** s[1:] - 10 ** s[:-1]
    s = (s[1:] + s[:-1]) / 2
    count_eucl = 10 ** np.concatenate(
        (poly(coef2)(s[s < s2]), poly(coef)(s[s >= s2]+3))
    )
    # poly_fit(s[s>=s2]+3,a,b,c,d,e,f,g,h)))
    s = 10**s
    count = count_eucl / s**2.5

    return s, count, ds


# Define source count model at 15.7 GHz
def calc_counts15700():
    # Power-law fit to 9C & 10C counts from Davies+2011, valid between 2.2 mJy - 1.0 Jy
    # y = a + (b+2.5)*x
    # where y = log10(dN/dS S^2.5) and x = log10(S)

    # Power-law fit to 10C & 10C ultradeep counts from Whittam+2016, valid between 0.2 - 2.2 mJy
    # y = a2 + (b2+2.5)*x

    # 7th order polynomial fit to 150 MHz model counts from Bonaldi+2019, valid between 10 nJy - 0.2 mJy
    # y = a3 + b3*x + c3*x**2 + d3*x**3 +e3*x**4 + f3*x**5 + g3*x**6 + h3*x**7

    # Set flux boundaries
    s1 = np.log10(1.0)
    s2 = np.log10(2.2e-3)
    s3 = -4.0
    s4 = -9.9

    # Parameters of polynomial fit to Davies+2011 counts
    coef = [np.log10(48.0), -2.13 + 2.5]

    # Parameters of power-law fit to Whittam+2016 counts
    coef2 = [np.log10(376.0), -1.80 + 2.5]

    # Parameters of polynomial fit to Bonaldi+2019 counts
    coef3 = [1.1554e01, 1.6112e01, 1.0659e01, 3.9388e00, 8.3476e-01, 1.0048e-01, 6.3498e-03, 1.6316e-04]

    # Calculate counts
    s = np.linspace(s4, s1, 1000)
    ds = 10 ** s[1:] - 10 ** s[:-1]
    s = (s[1:] + s[:-1]) / 2
    count_eucl = 10 ** np.concatenate(
        (
            poly(coef3)(s[s < s3]),
            poly(coef2)(s[(s >= s3) & (s < s2)]),
            poly(coef)(s[s >= s2]),
        )
    )
    s = 10**s
    count = count_eucl / s**2.5

    return s, count, ds


# Estimate rms noise from interquartile range of P(D) distribution
def stats_hist(x, y):
    y_sum = np.cumsum(y) / np.sum(y)
    q1 = x[(np.abs(y_sum - 0.25)).argmin()]
    q3 = x[(np.abs(y_sum - 0.75)).argmin()]
    sigma = (q3 - q1) / 1.349
    return sigma, q1, q3


def twodgaussian(inpars, circle=False, rotate=True, vheight=True, shape=None):
    """
    Returns a 2d gaussian function of the form:
    x' = np.cos(rota) * x - np.sin(rota) * y
    y' = np.sin(rota) * x + np.cos(rota) * y
    (rota should be in degrees)
    g = b + a * np.exp ( - ( ((x-center_x)/width_x)**2 +
    ((y-center_y)/width_y)**2 ) / 2 )
    inpars = [b,a,center_x,center_y,width_x,width_y,rota]
             (b is background height, a is peak amplitude)
    where x and y are the input parameters of the returned function,
    and all other parameters are specified by this function
    However, the above values are passed by list.  The list should be:
    inpars = (height,amplitude,center_x,center_y,width_x,width_y,rota)
    You can choose to ignore / neglect some of the above input parameters using
    the following options:
    Parameters
    ----------
    circle : bool
        default is an elliptical gaussian (different x, y widths), but can
        reduce the input by one parameter if it's a circular gaussian
    rotate : bool
        default allows rotation of the gaussian ellipse.  Can
        remove last parameter by setting rotate=0
    vheight : bool
        default allows a variable height-above-zero, i.e. an
        additive constant for the Gaussian function.  Can remove first
        parameter by setting this to 0
    shape : tuple
        if shape is set (to a 2-parameter list) then returns an image with the
        gaussian defined by inpars
    """
    inpars_old = inpars
    inpars = list(inpars)
    if vheight:
        height = inpars.pop(0)
        height = float(height)
    else:
        height = float(0)
    amplitude, center_y, center_x = inpars.pop(0), inpars.pop(0), inpars.pop(0)
    amplitude = float(amplitude)
    center_x = float(center_x)
    center_y = float(center_y)
    if circle:
        width = inpars.pop(0)
        width_x = float(width)
        width_y = float(width)
        rotate = 0
    else:
        width_x, width_y = inpars.pop(0), inpars.pop(0)
        width_x = float(width_x)
        width_y = float(width_y)
    if rotate:
        rota = inpars.pop(0)
        rota = np.pi / 180.0 * float(rota)
        rcen_x = center_x * np.cos(rota) - center_y * np.sin(rota)
        rcen_y = center_x * np.sin(rota) + center_y * np.cos(rota)
    else:
        rcen_x = center_x
        rcen_y = center_y
    if len(inpars) > 0:
        raise ValueError(
            "There are still input parameters:"
            + str(inpars)
            + " and you've input: "
            + str(inpars_old)
            + " circle=%d, rotate=%d, vheight=%d" % (circle, rotate, vheight)
        )

    def rotgauss(x, y):
        if rotate:
            xp = x * np.cos(rota) - y * np.sin(rota)
            yp = x * np.sin(rota) + y * np.cos(rota)
        else:
            xp = x
            yp = y
        g = height + amplitude * np.exp(
            -(((rcen_x - xp) / width_x) ** 2 + ((rcen_y - yp) / width_y) ** 2)
            / 2.0
        )
        return g

    if shape is not None:
        return rotgauss(*np.indices(shape))
    else:
        return rotgauss


# Construct the CLEAN beam
def clean_beam(cbmajas, cbminas):
    cbmajd = cbmajas / 3600
    cbmind = cbminas / 3600
    nx = 100  # set size of CLEAN beam array
    cx = nx / 2 - 1  # set centre
    pxszd = cbmind / 5  # set cell size to 1/5th of beam minor axis
    pxszas2 = (pxszd * 3600) ** 2  # cell area in arcsec^2
    sigma_factor = 2 * np.sqrt(2 * np.log(2))
    cbmajpsd = cbmajd / (
        pxszd * sigma_factor
    )  # calculate sigma of beam major axis in units of pixels
    cbminpsd = cbmind / (
        pxszd * sigma_factor
    )  # calculate sigma of beam minor axis in units of pixels
    pars3 = [1.0, cx, cx, cbmajpsd, cbminpsd, 0.0]
    cbm = twodgaussian(pars3, rotate=True, vheight=False, shape=[nx, nx])
    return cbm, pxszas2


# Calculate predicted P(D) given a source count and image parameters
def onebm(
    pxsz,
    dnn,
    logs,
    logdnds,
    mins,
    maxs,
    minx,
    maxx,
    beam,
    sigma,
    meanx=0.0,
    bvmin=1.0e-6,
    summean=False,
):

    pofd = 0.0

    assr = 4.25452e10
    pii = np.pi
    # PIXEL SIZE IN STERADIANS
    pxsz_sr = pxsz / assr

    # LOG(S), DNDS, LOG(DNDS)
    ss = 10.0**logs
    # SCALE COUNTS TO PER PIXEL INSTEAD OF PER SR
    logdnds_px = logdnds + np.log10(pxsz_sr)

    # SETUP D ARRAY
    # dnn = nd #SIZE OF D ARRAY SOMETHING LARGE FOR GOOD RESOLUTION
    dx = (maxx - minx) / (dnn - 1.0)
    dd = np.arange(minx, maxx + dx / 2.0, dx)
    dd2 = (dd[1:] + dd[:-1]) / 2.0
    dnn1 = len(dd)

    # SETUP BEAM ARRAY (ASSUME GAUSSIAN)
    jl = beam.shape
    jll = len(jl)
    if jll == 2:
        beam1 = beam.reshape(beam.size)
    if jll == 1:
        beam1 = beam.copy()
    beam1 = beam1[beam1 >= bvmin]  # SET SOME LOWER LIMIT
    beam11, bcount = np.unique(beam1, return_counts=True)
    nb = len(beam11)

    # DO RX INTEGRAL
    rx = np.zeros(dnn)
    xd = (np.arange(dnn) + 1) * dx
    inti = (np.log10(xd.max()) - np.log10(dx)) / 500.0
    xdintl = np.arange(np.log10(dx), np.log10(xd.max() + inti / 2.0), inti)
    xdint = 10.0**xdintl
    ni = xdint.size
    rxint = np.zeros(ni)
    for i in range(ni):
        flux = xdint[i] / beam11
        flux1 = flux[(flux >= mins) & (flux < maxs)]
        beam2 = beam11[(flux >= mins) & (flux < maxs)]
        bcnt2 = bcount[(flux >= mins) & (flux < maxs)]
        logflx = np.log10(flux1)
        newy = np.interp(logflx, logs, logdnds_px)
        rxint[i] = sum((10.0**newy / beam2) * bcnt2)

    rxintl = np.log10(rxint)
    rxintl[rxint == 0] = -10.0
    rx = 10.0 ** np.interp(np.log10(xd), xdintl, rxintl)

    # COMPUTE FFTS
    frx0 = rx * dx
    frx1 = fftpack.fft(frx0)
    frx2 = frx1 - frx1[0]

    omega = fftpack.fftfreq(dnn, dx) * np.pi * 2.0

    ### IF Want to estimate mean ####
    if summean == True:
        mapmean = sum(xd * rx) * dx + np.argmin(abs(dd)) * dx
        mapmeani = complex(0, mapmean)
        frx2 = frx2 - (omega * mapmeani)
    ##IF YOU WANT TO ADD NOISE
    frx3 = frx2 - (sigma**2 * omega**2) / 2.0

    frx4 = np.exp(frx3)

    pofd = np.real(fftpack.ifft(frx4))

    cs = int(((meanx - dd[0]) / dx))
    aa = np.argmax(pofd)
    sh = cs - aa
    if dd[0] != 0:
        pofd = np.roll(pofd, sh)

    return pofd, dd


# Calculate P(D) distribution and extract confusion noise
def calc_pofd(
    cbmajas,
    cbminas,
    cbm,
    pxszas2,
    s,
    count,
    ds,
    nd_index=18,
    zero_shift=True,
    bmin=1e-5,
):

    print("Calculating source P(D) distribution")
    nd = 2**nd_index  # Calculate total number of bins
    sterad = (180 / np.pi * 3600) ** 2  # sterad in arcsec^2
    barea_factor = np.pi / (4 * np.log(2))
    beamar = barea_factor * cbmajas * cbminas / sterad  # beam area in sterad
    nsrc = np.zeros(count.size)
    for i in range(count.size):
        nsrc[i] = (count * ds)[i:].sum()
    """ Calculate the flux density, sref, above which there are 20 beams/source. 
    This quantity should be roughly proportional to the confusion noise
    and can be used to set the mininum and maximum deflections """
    sref = np.interp((1 / 20), (nsrc * beamar)[::-1], s[::-1])
    """ Set the maximum deflection, xmax = 10*sref
    This should be large enough to ensure that pixel values above xmax have a negligible
    effect on the confusion noise """
    xmax = sref * 10
    xmin = -xmax / 5
    dx = (xmax - xmin) / nd  # calculate the bin size
    print(
        "Flux density above which there are 20 beams/source =", "%.2e" % sref, "Jy",
        "\nMinimum deflection =", "%.2e" % xmin, "Jy/beam",
        "\nMaximum deflection =", "%.2e" % xmax, "Jy/beam",
        "\nBin size =", "%.2e" % dx, "Jy/beam",
    )
    smin = s.min()
    smax = s.max()

    # Calculate P(D). The peak of the distribution will be centred at D = 0.
    pofd, d = onebm(
        pxszas2,
        nd,
        np.log10(s),
        np.log10(count),
        smin,
        smax,
        xmin,
        xmax,
        cbm,
        0.0,
        meanx=0.0,
        bvmin=bmin,
        summean=False,
    )

    # Calculate shift in x direction to apply to the distribution to ensure that P(D<0) ~ 0.
    # Re-calculate P(D) applying the shift (this has no effect on the width of the distribution).
    if zero_shift:
        max_pofd = np.amax(pofd)
        i = np.argmax(pofd)
        while pofd[i] > max_pofd / 100 and i >= 0:
            i = i - 1
        xzero = d[i]
        pofd, d = onebm(
            pxszas2,
            nd,
            np.log10(s),
            np.log10(count),
            smin,
            smax,
            xmin,
            xmax,
            cbm,
            0.0,
            meanx=-xzero,
            bvmin=bmin,
            summean=False,
        )

    # Normalise to measure a PDF (this ensures that area beneath PDF is equal to 1.0)
    pofd = pofd / dx

    # Calculate sigma
    sigma, q1, q3 = stats_hist(d, pofd)
    print("Confusion noise =", "%.2e" % sigma, "Jy/beam")

    return d, pofd, sigma, q1, q3


# Beam sizes, in arcsec
beam = 10 ** np.linspace(np.log10(1.5), np.log10(7000), 100)
bmaj, bmin = [beam] * 2  # assume circular beam
beam = np.stack((bmaj, bmin, np.sqrt(bmaj * bmin)), axis=-1)
sterad = (180 / np.pi * 3600) ** 2  # sterad in arcsec^2
barea_factor = np.pi / (4 * np.log(2))
freq = np.array([150, 1400, 15700])
nf = len(freq)
nb = len(beam)
sigma_list, upper_lim = np.empty([nf, nb]), np.zeros([nf, nb]).astype(int)

# Loop over source count frequencies
for i in range(0, nf):
    print("Source count frequency (MHz) =", freq[i])
    if freq[i] == 1400:
        s, count, ds = calc_counts1400()
    elif freq[i] == 150:
        s, count, ds = calc_counts150()
    elif freq[i] == 15700:
        s, count, ds = calc_counts15700()
    else:
        print("Error: no source counts at specified frequency. Aborting.")
        exit()
    ntot = (count * ds)[
        0:
    ].sum()  # calculate total # of sources/sterad in the source count
    barea_min = (
        sterad / ntot
    )  # set minimum beam area such that there is at least one source per beam

    # Loop over beams
    for j in range(0, nb):
        print("Beam size (arcsec) =", "%.2f" % beam[j, 2])

        # Check beam size
        cbmajas = beam[j, 0]
        cbminas = beam[j, 1]
        barea = barea_factor * beam[j, 2] ** 2  # beam area in arcsec^2
        if barea < barea_min:
            cbmajas = np.sqrt(barea_min / barea_factor)
            cbminas = cbmajas
            print(
                "Cannot calculate the confusion noise given the available"
                " source count model.",
                "\nThe smallest allowed beam size is",
                "%.3f" % cbmajas,
                "arcsec.",
                "\nThe confusion noise will be calculated instead for a beam"
                " size of",
                "%.3f" % cbmajas,
                "arcsec.",
                "\nIt should be taken as an upper limit.",
            )
            upper_lim[i, j] = 1

        # Generate CLEAN beam
        cbm, pxszas2 = clean_beam(cbmajas, cbminas)

        # Calculate P(D) distribution and extract confusion noise
        x, y, sigma, q1, q3 = calc_pofd(
            cbmajas, cbminas, cbm, pxszas2, s, count, ds
        )
        sigma_list[i, j] = sigma


# Remove beams that do not have confusion noise values at all frequencies
beam_sel = []
sigma_sel = []
freq_sel = []
for i in range(0, nb):
    if np.all((upper_lim[:, i] == 0)):
        beam_sel.append(beam[i, 2]*u.arcsec.to(u.deg))
        freq_sel.append(freq*1e6)
        sigma_sel.append(sigma_list[:, i])
beam_sel = np.array(beam_sel)
sigma_sel = np.array(sigma_sel)

# saving data as an astropy data table
table = QTable([beam_sel, freq_sel, sigma_sel], names=("beam", "frequency", "sigma"), units=(u.deg, u.Hz, u.Jy))
table.write(output_file, overwrite=True)
