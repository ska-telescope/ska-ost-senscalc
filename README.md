SKA Sensitivity Calculator
==========================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ost-senscalc/badge/?version=latest)](https://developer.skao.int/projects/ska-ost-senscalc/en/latest/?badge=latest)

The Sensitivity Calculator is an application for calculating the sensitivity 
of SKA MID and LOW.

This project contains the backend of the application: a Flask server defined by an OpenAPI spec, 
which offers resources to calculate the sensitivity or integration time based on input parameters. 

The front end of the application is split into a separate project, 
[ska-ost-senscalc-ui](https://gitlab.com/-/ide/project/ska-telescope/ost/ska-ost-senscalc-ui).

Below are instructions on building, testing and deploying the backend application.

# Local development and deployment

To clone this repository, run

```
git clone --recurse-submodules git@gitlab.com:ska-telescope/ost/ska-ost-senscalc.git
```

To refresh the GitLab Submodule, execute below commands:

```
git submodule update --recursive --remote
git submodule update --init --recursive
```

Install dependencies with Poetry and activate the virtual environment

```
poetry install
poetry shell
```

To build a new Docker image for the Sensitivity Calculator, run

```
make oci-build
```

Execute the unit test suite and lint the project with:

```
make python-test
make python-lint
```

## Deploying to Kubernetes
  
```  
make k8s-install-chart  
```  
  
and uninstall the chart with  
```  
make k8s-uninstall-chart  
```

Once installed, the Sensitivity Calculator API should be available externally to the Kubernetes cluster. 
If using minikube, run `minikube ip` and use the IP to access the API, eg 

```
http://<KUBE_HOST>/<KUBE_NAMESPACE>/senscalc/api/v<MAJOR_VERSION>/mid/subarrays. 
```

The Swagger UI for the Mid and Low API should be available at 

```
http://<KUBE_HOST>/<KUBE_NAMESPACE>/senscalc/api/v<MAJOR_VERSION>/mid/ui/
http://<KUBE_HOST>/<KUBE_NAMESPACE>/senscalc/api/v<MAJOR_VERSION>/low/ui/
```


Execute the component tests in k8s environment with:

```
make k8s-install-chart && make k8s-wait
make k8s-test
make k8s-uninstall-chart
```

# Deployments from CICD

## Deploying to non-production environments

There are 3 different environments which are defined through the standard pipeline templates. They need to be manually triggered in the Gitlab UI.

1. `dev` - a temporary (4 hours) deployment from a feature branch, using the artefacts built in the branch pipeline
2. `integration` - a permanent deployment from the main branch, using the latest version of the artefacts built in the main pipeline
3. `staging` - a permanent deployment of the latest published artefact from CAR

To find the URL for the environment, see the 'info' job of the CICD pipeline stage, which should output the URL alongside the status of the Kubernetes pods.
Generally the API URL should be available at  `https://k8s.stfc.skao.int/$KUBE_NAMESPACE/api/`

## Deploying to Production environment

The deployment to production is controlled by the https://gitlab.com/-/ide/project/ska-telescope/ost/ska-ost-senscalc-ui as it has both the frontend and backend deployments in its umbrella chart.

### Restrictions

The production environment is called `production-ska-ost-senscalc` in GitLab's UI and can be seen from here: https://gitlab.com/ska-telescope/ost/ska-ost-senscalc/-/environments/11337767
Only the maintainers can make a deployment and all deployments should be approved from the above interface when a new job is triggered for deployment.
These settings can be changed by the project owner or the team-system if needed.

# Further help

To rebuild the PlantUML and drawio diagrams after modification, from a
non-interactive session run

```
make diagrams
```

Further documentation, including a ``User Guide`` can be found in the 
``docs`` folder, or on [ReadTheDocs](https://developer.skao.int/projects/ska-ost-senscalc/en/latest/?badge=latest). 

To build the html version of the documentation, start 
from the ``ska-ost-senscalc`` directory and first install the dependency using 
``poetry install --only docs`` and then type ``make docs-build html``. Read the documentation by pointing your browser
at ``docs/build/html/index.html``.

