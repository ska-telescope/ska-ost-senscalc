from pathlib import Path

import ska_ost_senscalc

STATIC_DATA_PATH = (
    Path(__file__).resolve().parent.parent / "src/ska_ost_senscalc/static"
)

OPEN_API = ska_ost_senscalc.get_openapi_spec("./static/openapi-mid.yaml")


class ErrorMessages:
    MAX_BANDWIDTH_FOR_SUBARRAY = (
        lambda maximum_bandwidth: f"Maximum bandwidth ({maximum_bandwidth} MHz) for this subarray has been exceeded."
    )
    SPECTRAL_WINDOW = "Spectral window defined by central frequency and bandwidth does not lie within the band range."
    SUBARRAY_FOR_BAND = "Subarray configuration not allowed for given observing band."
    INVALID_POINTING_CENTRE = "Specified pointing centre is invalid, expected format HH:MM:SS[.ss] DD:MM:SS[.ss]."


class LowErrorMessages(ErrorMessages):
    ZOOM_TOTAL_BANDWIDTH = (
        lambda total_bandwidth_khz, subarray_configuration: f"Bandwidth {total_bandwidth_khz} not one the of allowed values in zoom mode for {subarray_configuration} subarray configuration: [24.4, 48.8, 97.7, 195.3, 390.6, 781.2, 1562.5, 3125.0]"
    )
    ZOOM_TOTAL_BANDWIDTH_SUBSET = (
        lambda total_bandwidth_khz, subarray_configuration: f"Bandwidth {total_bandwidth_khz} not one the of allowed values in zoom mode for {subarray_configuration} subarray configuration: [390.6, 781.2, 1562.5, 3125.0]"
    )
    ZOOM_SPECTRAL_WINDOW = "Spectral window defined by central frequency and bandwidth does not lie within the 50 - 350 MHz range."
    ALWAYS_BELOW_HORIZON = (
        "Specified pointing centre is always below the horizon from the SKA LOW site"
    )
    INVALID_SPECTRAL_AVERAGING_FACTOR = (
        "The spectral averaging factor must lie between 1 and {}".format
    )
    SUBARRAY_OR_NUM_STATIONS = (
        "Only 'subarray_configuration' or 'num_stations' should be specified."
    )
    N_SUBBANDS_FOR_SUBBAND_FREQ_CENTRES = "When subband_freq_centres_mhz is provided, Parameter 'n_subbands' should be set and match the number of subband_freq_centres_mhz."
