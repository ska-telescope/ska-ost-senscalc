"""
Unit tests for the ska_ost_senscalc/common/service.py module
"""

import json
from http import HTTPStatus

import pytest
import requests

from ska_ost_senscalc.common.service import SENS_LIMIT_WARNING
from tests.component import LOW_API_URL


class TestTransformations:
    def test_calculate_continuum_results_low_aa4(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
        }
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK

        expected = {
            "calculate": {
                "continuum_sensitivity": {
                    "value": 4.7783942488688345,
                    "unit": "uJy / beam",
                },
                "continuum_subband_sensitivities": [],
                "spectral_sensitivity": {
                    "value": 666.5608692364152,
                    "unit": "uJy / beam",
                },
                "warnings": [],
                "spectropolarimetry_results": {
                    "fwhm_of_the_rmsf": {
                        "value": 0.09837671426218081,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth_extent": {
                        "value": 4.28191284692133,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth": {
                        "value": 222.0457075444506,
                        "unit": "rad / m2",
                    },
                },
            },
            "weighting": {
                "continuum_weighting": {
                    "weighting_factor": 10.331757457585987,
                    "sbs_conv_factor": 3624408.8848622595,
                    "subbands": [],
                    "confusion_noise": {
                        "value": 6.328070885097382e-07,
                        "limit_type": "value",
                    },
                    "beam_size": {
                        "beam_maj_scaled": 0.0009294689852640001,
                        "beam_min_scaled": 0.0006998047590761798,
                        "beam_pa": 190.26451545511534,
                    },
                },
                "spectral_weighting": {
                    "weighting_factor": 12.524796556896934,
                    "sbs_conv_factor": 1338319.6546982538,
                    "subbands": [],
                    "confusion_noise": {
                        "value": 2.560245257439271e-06,
                        "limit_type": "value",
                    },
                    "beam_size": {
                        "beam_maj_scaled": 0.0014414588620446558,
                        "beam_min_scaled": 0.0012220440815741277,
                        "beam_pa": 10.089813757113648,
                    },
                },
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": {
                    "value": 4.9369210416036565e-05,
                    "unit": "Jy / beam",
                },
                "continuum_confusion_noise": {
                    "value": 6.328070885097382e-07,
                    "unit": "Jy",
                },
                "total_continuum_sensitivity": {
                    "value": 4.937326586234864e-05,
                    "unit": "Jy / beam",
                },
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 3.3460883469504004, "unit": "arcsec"},
                    "beam_min": {"value": 2.519297132674247, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": {
                    "value": 178.9489034661629,
                    "unit": "K",
                },
                "weighted_spectral_sensitivity": {
                    "value": 0.00834853927997448,
                    "unit": "Jy / beam",
                },
                "spectral_confusion_noise": {
                    "value": 2.560245257439271e-06,
                    "unit": "Jy",
                },
                "total_spectral_sensitivity": {
                    "value": 0.00834853967254948,
                    "unit": "Jy / beam",
                },
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 5.189251903360761, "unit": "arcsec"},
                    "beam_min": {"value": 4.399358693666859, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": {
                    "value": 11173.014731801093,
                    "unit": "K",
                },
                "continuum_integration_time": None,
                "spectral_integration_time": None,
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        assert expected == res

    def test_calculate_continuum_results_low_aa4_core_only(self):
        params = {
            "subarray_configuration": "LOW_AA4_core_only",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
        }
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK

        expected = {
            "calculate": {
                "continuum_sensitivity": {
                    "value": 10.935810321200286,
                    "unit": "uJy / beam",
                },
                "continuum_subband_sensitivities": [],
                "spectral_sensitivity": {
                    "value": 1525.4880308859829,
                    "unit": "uJy / beam",
                },
                "warnings": [],
                "spectropolarimetry_results": {
                    "fwhm_of_the_rmsf": {
                        "value": 0.09837671426218081,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth_extent": {
                        "value": 4.28191284692133,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth": {
                        "value": 222.0457075444506,
                        "unit": "rad / m2",
                    },
                },
            },
            "weighting": {
                "continuum_weighting": {
                    "weighting_factor": 7.027875897434313,
                    "sbs_conv_factor": 1113.6494464810996,
                    "subbands": [],
                    "confusion_noise": {
                        "value": 0.0028963639869200083,
                        "limit_type": "value",
                    },
                    "beam_size": {
                        "beam_maj_scaled": 0.04644774997785056,
                        "beam_min_scaled": 0.045575956758137644,
                        "beam_pa": -42.17201236653855,
                    },
                },
                "spectral_weighting": {
                    "weighting_factor": 2.673500228274607,
                    "sbs_conv_factor": 799.5658780247327,
                    "subbands": [],
                    "confusion_noise": {
                        "value": 0.004463884883645717,
                        "limit_type": "value",
                    },
                    "beam_size": {
                        "beam_maj_scaled": 0.054895574765788,
                        "beam_min_scaled": 0.05371027698592528,
                        "beam_pa": -215.62564780088655,
                    },
                },
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": {
                    "value": 7.685551777527687e-05,
                    "unit": "Jy / beam",
                },
                "continuum_confusion_noise": {
                    "value": 0.0028963639869200083,
                    "unit": "Jy",
                },
                "total_continuum_sensitivity": {
                    "value": pytest.approx(0.002897383494696494, rel=1e-17),
                    "unit": "Jy / beam",
                },
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 167.21189992026203, "unit": "arcsec"},
                    "beam_min": {"value": 164.0734443292955, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": {
                    "value": pytest.approx(3.2266695251122246),
                    "unit": "K",
                },
                "weighted_spectral_sensitivity": {
                    "value": 0.004078392598803856,
                    "unit": "Jy / beam",
                },
                "spectral_confusion_noise": {
                    "value": 0.004463884883645717,
                    "unit": "Jy",
                },
                "total_spectral_sensitivity": {
                    "value": 0.00604644973884831,
                    "unit": "Jy / beam",
                },
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 197.62406915683678, "unit": "arcsec"},
                    "beam_min": {"value": 193.35699714933102, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": {
                    "value": 4.834534894374665,
                    "unit": "K",
                },
                "continuum_integration_time": None,
                "spectral_integration_time": None,
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [
                    {"continuum_sensitivity": SENS_LIMIT_WARNING},
                    {"spectral_sensitivity": SENS_LIMIT_WARNING},
                ],
            },
        }

        assert expected == res

    def test_calculate_continuum_results_low_aa4_star_all(self):
        params = {
            "subarray_configuration": "LOW_AAstar_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
        }
        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)

        res = json.loads(response.content)

        assert response.status_code == HTTPStatus.OK

        expected = {
            "calculate": {
                "continuum_sensitivity": {
                    "value": 7.974390677834406,
                    "unit": "uJy / beam",
                },
                "continuum_subband_sensitivities": [],
                "spectral_sensitivity": {
                    "value": 1112.3855640639865,
                    "unit": "uJy / beam",
                },
                "warnings": [],
                "spectropolarimetry_results": {
                    "fwhm_of_the_rmsf": {
                        "value": 0.09837671426218081,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth_extent": {
                        "value": 4.28191284692133,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth": {
                        "value": 222.0457075444506,
                        "unit": "rad / m2",
                    },
                },
            },
            "weighting": {
                "continuum_weighting": {
                    "weighting_factor": 10.193990989271752,
                    "sbs_conv_factor": 3253529.6874249526,
                    "subbands": [],
                    "confusion_noise": {
                        "value": 7.447430164965799e-07,
                        "limit_type": "value",
                    },
                    "beam_size": {
                        "beam_maj_scaled": 0.0009482680137895635,
                        "beam_min_scaled": 0.0007641227064772109,
                        "beam_pa": 375.66579891311113,
                    },
                },
                "spectral_weighting": {
                    "weighting_factor": 14.830079445303223,
                    "sbs_conv_factor": 1419257.4540887652,
                    "subbands": [],
                    "confusion_noise": {
                        "value": 2.372411064047605e-06,
                        "limit_type": "value",
                    },
                    "beam_size": {
                        "beam_maj_scaled": 0.0013844226826915209,
                        "beam_min_scaled": 0.0011998282703326215,
                        "beam_pa": 18.43167627398016,
                    },
                },
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": {
                    "value": 8.12908667147766e-05,
                    "unit": "Jy / beam",
                },
                "continuum_confusion_noise": {
                    "value": 7.447430164965799e-07,
                    "unit": "Jy",
                },
                "total_continuum_sensitivity": {
                    "value": 8.129427810984113e-05,
                    "unit": "Jy / beam",
                },
                "continuum_synthesized_beam_size": {
                    "beam_maj": {"value": 3.4137648496424284, "unit": "arcsec"},
                    "beam_min": {"value": 2.7508417433179595, "unit": "arcsec"},
                },
                "continuum_surface_brightness_sensitivity": {
                    "value": 264.4933472481486,
                    "unit": "K",
                },
                "weighted_spectral_sensitivity": {
                    "value": 0.01649676628887736,
                    "unit": "Jy / beam",
                },
                "spectral_confusion_noise": {
                    "value": 2.372411064047605e-06,
                    "unit": "Jy",
                },
                "total_spectral_sensitivity": {
                    "value": pytest.approx(0.01649676645946637, rel=1e-2),
                    "unit": "Jy / beam",
                },
                "spectral_synthesized_beam_size": {
                    "beam_maj": {"value": 4.983921657689475, "unit": "arcsec"},
                    "beam_min": {"value": 4.3193817731974375, "unit": "arcsec"},
                },
                "spectral_surface_brightness_sensitivity": {
                    "value": pytest.approx(23413.158765959175, abs=1),
                    "unit": "K",
                },
                "continuum_integration_time": None,
                "spectral_integration_time": None,
                "confusion_noise_per_subband": None,
                "synthesized_beam_size_per_subband": None,
                "integration_time_per_subband": None,
                "warnings": [],
            },
        }

        assert expected == res

    def test_invalid_spectral_window(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 300,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
        }

        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Spectral window defined by central frequency and bandwidth does not lie within the 50 - 350 MHz range.",
        }
        assert expected == res

    def test_invalid_pointing_centre(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 90,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
        }

        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "Specified pointing centre is always below the horizon from the SKA LOW site",
        }
        assert expected == res

    def test_subband_frequencies_centers_not_matching_n_subbands(self):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
            "subband_freq_centres_mhz": [150, 120],
        }

        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        res = json.loads(response.content)

        expected = {
            "title": "Validation Error",
            "detail": "When subband_freq_centres_mhz is provided, Parameter 'n_subbands' should be set and match the number of subband_freq_centres_mhz.",
        }
        assert expected == res

    def test_one_n_subband_without_subband_frequ_center_generates_a_valid_response_with_no_subbands(
        self,
    ):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
            "n_subbands": 1,
        }

        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        res = json.loads(response.content)

        assert [] == res["calculate"]["continuum_subband_sensitivities"]
        assert res["weighted_result"].get("weighted_sensitivity_per_subband") is None
        assert res["weighted_result"].get("confusion_noise_per_subband") is None
        assert res["weighted_result"].get("total_sensitivity_per_subband") is None
        assert res["weighted_result"].get("synthesized_beam_size_per_subband") is None
        assert (
            res["weighted_result"].get("surface_brightness_sensitivity_per_subband")
            is None
        )

    def test_one_n_subband_with_one_subband_frequ_center_generates_a_valid_response_with_one_subband(
        self,
    ):
        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
            "n_subbands": 1,
            "subband_freq_centres_mhz": [150],
        }

        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        res = json.loads(response.content)

        continuum_subband_sensitivities = [
            {
                "subband_freq_centre": {"value": 200, "unit": "MHz"},
                "sensitivity": {"value": 4.7783942488688345, "unit": "uJy / beam"},
            }
        ]
        assert (
            continuum_subband_sensitivities
            == res["calculate"]["continuum_subband_sensitivities"]
        )

        subbands = [
            {
                "subband_freq_centre": {"value": 150000000, "unit": "Hz"},
                "weighting_factor": 10.331757457585987,
                "sbs_conv_factor": 3624408.8848622595,
                "confusion_noise": {
                    "value": 0.0000017546386703923204,
                    "limit_type": "value",
                },
                "beam_size": {
                    "beam_maj_scaled": 0.001239291980352,
                    "beam_min_scaled": 0.000933073012101573,
                    "beam_pa": 190.26451545511534,
                },
            }
        ]
        assert subbands == res["weighting"]["continuum_weighting"]["subbands"]

        weighted_sensitivity_per_subband = {
            "min_value": {"value": 0.000049369210416036565, "unit": "Jy / beam"},
            "max_value": {"value": 0.000049369210416036565, "unit": "Jy / beam"},
        }
        assert (
            weighted_sensitivity_per_subband
            == res["weighted_result"]["weighted_sensitivity_per_subband"]
        )

        confusion_noise_per_subband = {
            "min_value": {"value": 0.0000017546386703923204, "unit": "Jy"},
            "max_value": {"value": 0.0000017546386703923204, "unit": "Jy"},
        }
        assert (
            confusion_noise_per_subband
            == res["weighted_result"]["confusion_noise_per_subband"]
        )

        total_sensitivity_per_subband = {
            "min_value": {"value": 0.00004940038151640662, "unit": "Jy / beam"},
            "max_value": {"value": 0.00004940038151640662, "unit": "Jy / beam"},
        }
        assert (
            total_sensitivity_per_subband
            == res["weighted_result"]["total_sensitivity_per_subband"]
        )

        synthesized_beam_size_per_subband = {
            "min_value": {
                "beam_maj": {"unit": "arcsec", "value": 4.461451129267201},
                "beam_min": {"unit": "arcsec", "value": 3.359062843565663},
            },
            "max_value": {
                "beam_maj": {"unit": "arcsec", "value": 4.461451129267201},
                "beam_min": {"unit": "arcsec", "value": 3.359062843565663},
            },
        }
        assert (
            synthesized_beam_size_per_subband
            == res["weighted_result"]["synthesized_beam_size_per_subband"]
        )

        surface_brightness_sensitivity_per_subband = {
            "min_value": {"value": 179.0471816836495, "unit": "K"},
            "max_value": {"value": 179.0471816836495, "unit": "K"},
        }
        assert (
            surface_brightness_sensitivity_per_subband
            == res["weighted_result"]["surface_brightness_sensitivity_per_subband"]
        )

    def test_generate_subbands_when_only_n_subbands_provided_and_check_subbands_results(
        self,
    ):
        n_subbands = 2
        params = {
            "subarray_configuration": "LOW_AAstar_all",
            "integration_time_h": 0.1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
            "n_subbands": n_subbands,
        }

        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        res = json.loads(response.content)

        assert n_subbands == len(res["calculate"]["continuum_subband_sensitivities"])
        assert n_subbands == len(res["weighting"]["continuum_weighting"]["subbands"])

        continuum_subband_sensitivities = [
            {
                "subband_freq_centre": {"value": 125, "unit": "MHz"},
                "sensitivity": {"value": 45.186841704691446, "unit": "uJy / beam"},
            },
            {
                "subband_freq_centre": {"value": 275, "unit": "MHz"},
                "sensitivity": {"value": 23.961316500917956, "unit": "uJy / beam"},
            },
        ]
        assert (
            continuum_subband_sensitivities
            == res["calculate"]["continuum_subband_sensitivities"]
        )

        subbands = [
            {
                "subband_freq_centre": {"value": 125000000, "unit": "Hz"},
                "weighting_factor": 10.193990989271752,
                "sbs_conv_factor": 3253529.6874249526,
                "confusion_noise": {
                    "value": 0.0000036996961166710036,
                    "limit_type": "value",
                },
                "beam_size": {
                    "beam_maj_scaled": 0.0015172288220633018,
                    "beam_min_scaled": 0.0012225963303635374,
                    "beam_pa": 375.66579891311113,
                },
            },
            {
                "subband_freq_centre": {"value": 275000000, "unit": "Hz"},
                "weighting_factor": 10.193990989271752,
                "sbs_conv_factor": 3253529.6874249536,
                "confusion_noise": {
                    "value": 2.2044595679752553e-7,
                    "limit_type": "value",
                },
                "beam_size": {
                    "beam_maj_scaled": 0.000689649464574228,
                    "beam_min_scaled": 0.0005557256047106988,
                    "beam_pa": 375.66579891311113,
                },
            },
        ]
        assert subbands == res["weighting"]["continuum_weighting"]["subbands"]

        weighted_sensitivity_per_subband = {
            "min_value": {"value": 0.0002442614445014462, "unit": "Jy / beam"},
            "max_value": {"value": 0.00046063425717127356, "unit": "Jy / beam"},
        }
        assert (
            weighted_sensitivity_per_subband
            == res["weighted_result"]["weighted_sensitivity_per_subband"]
        )

        confusion_noise_per_subband = {
            "min_value": {"value": 2.2044595679752553e-07, "unit": "Jy"},
            "max_value": {"value": 3.6996961166710036e-06, "unit": "Jy"},
        }
        assert (
            confusion_noise_per_subband
            == res["weighted_result"]["confusion_noise_per_subband"]
        )

        total_sensitivity_per_subband = {
            "min_value": {"value": 0.00024426154397766533, "unit": "Jy / beam"},
            "max_value": {
                "value": pytest.approx(0.00046064911443645113, rel=1e-19),
                "unit": "Jy / beam",
            },
        }
        assert (
            total_sensitivity_per_subband
            == res["weighted_result"]["total_sensitivity_per_subband"]
        )

        synthesized_beam_size_per_subband = {
            "min_value": {
                "beam_maj": {"unit": "arcsec", "value": 2.482738072467221},
                "beam_min": {"unit": "arcsec", "value": 2.0006121769585157},
            },
            "max_value": {
                "beam_maj": {"unit": "arcsec", "value": 5.462023759427886},
                "beam_min": {"unit": "arcsec", "value": 4.401346789308734},
            },
        }
        assert (
            synthesized_beam_size_per_subband
            == res["weighted_result"]["synthesized_beam_size_per_subband"]
        )

        surface_brightness_sensitivity_per_subband = {
            "min_value": {
                "value": pytest.approx(794.71218482759, abs=1e-2),
                "unit": "K",
            },
            "max_value": {
                "value": pytest.approx(1498.735569305008, abs=1e-2),
                "unit": "K",
            },
        }
        assert (
            surface_brightness_sensitivity_per_subband
            == res["weighted_result"]["surface_brightness_sensitivity_per_subband"]
        )

    def test_generate_4_subbands_when_4_n_subbands_provided_with_no_subband_freq_center(
        self,
    ):
        n_subbands = 4
        params = {
            "subarray_configuration": "LOW_AAstar_all",
            "integration_time_h": 0.1,
            "pointing_centre": "10:00:00.00 -30:00:00.00",
            "elevation_limit": 20,
            "freq_centre_mhz": 200,
            "spectral_averaging_factor": 1,
            "bandwidth_mhz": 300,
            "weighting_mode": "uniform",
            "robustness": 0,
            "n_subbands": n_subbands,
        }

        response = requests.get(f"{LOW_API_URL}/continuum/calculate", params=params)
        res = json.loads(response.content)

        assert n_subbands == len(res["calculate"]["continuum_subband_sensitivities"])
        assert n_subbands == len(res["weighting"]["continuum_weighting"]["subbands"])
