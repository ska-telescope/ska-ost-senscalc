from unittest import mock

import astropy.units as u
from astropy.coordinates import SkyCoord
from astropy.units import Quantity, deg, hourangle

from ska_ost_senscalc.common.model import (
    ContinuumRequest,
    MidBand,
    Weighting,
    ZoomRequestPrepared,
)
from ska_ost_senscalc.mid.calculator import DEFAULT_EL, DEFAULT_PWV
from ska_ost_senscalc.mid.service import (
    convert_continuum_input_and_calculate,
    get_subbands,
    get_zoom_calculate_response,
)
from ska_ost_senscalc.subarray import MIDArrayConfiguration
from ska_ost_senscalc.utilities import Telescope


def test_get_subbands():
    """This tests the get_subbands method for one set of inputs."""
    n_subbands = 3
    obs_freq = 0.7e9
    bandwidth = 0.4e9

    result = get_subbands(n_subbands, obs_freq, bandwidth)
    assert result == (
        [566666666.6666666, 700000000.0, 833333333.3333333],
        133333333.33333333,
    )


# Auxiliary classes to mock the Flask response
class MockArgList:
    def __init__(self, dict_args, dict_args_list=None):
        self._dict_args = dict_args
        if dict_args_list is not None:
            self._dict_args_list = dict_args_list

    def get(self, name):
        try:
            return self._dict_args[name]
        except KeyError:
            return None

    def getlist(self, name):
        try:
            return self._dict_args_list[name]
        except KeyError:
            return []


class MockRequest:
    """Class to mock the access to the arguments of a request"""

    def __init__(self, dict_args, dict_args_list=None):
        self.args = MockArgList(dict_args, dict_args_list=dict_args_list)


class TestContinuum:
    def test_get_calculate_response_sensitivity(self):
        continuum_request = ContinuumRequest(
            freq_centre_hz=797500000.0,
            rx_band="Band 1",
            bandwidth_hz=435000000.0,
            pointing_centre=SkyCoord(
                "00:00:00.0 00:00:00.0", frame="icrs", unit=(hourangle, deg)
            ),
            weighting_mode=Weighting.UNIFORM,
            subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
            robustness=0.0,
            n_subbands=1,
            subband_freq_centres=[],
            spectral_averaging_factor=1.0,
            integration_time_s=600.0,
            supplied_sensitivity=None,
            sensitivity_unit="Jy/beam",
            n_ska=None,
            n_meer=None,
            pwv=10.0,
            el=45.0,
            taper=Quantity(0.0, u.arcsec),
            freq_centre=Quantity(7.975e08, u.Hz),
            telescope=Telescope.MID,
            subband_freq_centres_hz=[],
            subband_supplied_sensitivities=[],
            subband_supplied_sensitivities_unit="Jy/beam",
            target=None,
        )

        result = convert_continuum_input_and_calculate(continuum_request)
        assert result["continuum_sensitivity"] > 0
        assert result["spectral_sensitivity"] > 0

    def test_get_calculate_response_subbands(self):
        continuum_request = ContinuumRequest(
            freq_centre_hz=797500000.0,
            rx_band="Band 1",
            bandwidth_hz=435000000.0,
            pointing_centre=SkyCoord(
                "00:00:00.0 00:00:00.0", frame="icrs", unit=(hourangle, deg)
            ),
            weighting_mode=Weighting.UNIFORM,
            subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
            robustness=0.0,
            n_subbands=3,
            subband_freq_centres=[],
            spectral_averaging_factor=1.0,
            integration_time_s=1.0,
            supplied_sensitivity=None,
            sensitivity_unit=u.Unit("Jy / beam"),
            pwv=10.0,
            el=45.0,
            alpha=2.75,
            taper=Quantity(unit=u.arcsec, value=0),
            freq_centre=Quantity(unit="Hz", value=6500000000),
            telescope=Telescope.MID,
            subband_freq_centres_hz=[
                Quantity(6.36666667e09, "Hz"),
                Quantity(6.5e09, "Hz"),
                Quantity(6.63333333e09, "Hz"),
            ],
            subband_supplied_sensitivities=[],
            subband_supplied_sensitivities_unit="Jy/beam",
            target=SkyCoord(ra=201.365 * deg, dec=-43.01916667 * deg, frame="icrs"),
        )

        result = convert_continuum_input_and_calculate(continuum_request)
        assert len(result["continuum_subband_sensitivities"]) == 3

        continuum_request = ContinuumRequest(
            freq_centre_hz=797500000.0,
            rx_band=MidBand.BAND_1.value,
            bandwidth_hz=435000000.0,
            pointing_centre=SkyCoord(
                "00:00:00.0 00:00:00.0", frame="icrs", unit=(hourangle, deg)
            ),
            weighting_mode=Weighting.UNIFORM,
            subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
            robustness=0.0,
            n_subbands=3,
            subband_freq_centres=[],
            spectral_averaging_factor=1.0,
            integration_time_s=None,
            supplied_sensitivity=0.001,
            sensitivity_unit="Jy/beam",
            n_ska=None,
            n_meer=None,
            pwv=DEFAULT_PWV,
            el=DEFAULT_EL.value,
            taper=Quantity(0.0, u.arcsec),
            freq_centre=Quantity(7.975e08, u.Hz),
            telescope=Telescope.MID,
            subband_freq_centres_hz=[
                Quantity(6.525e08, u.Hz),
                Quantity(7.975e08, u.Hz),
                Quantity(9.425e08, u.Hz),
            ],
            subband_supplied_sensitivities=[1.0, 2.0, 3.0],
            subband_supplied_sensitivities_unit="Jy/beam",
            target=None,
        )

        result = convert_continuum_input_and_calculate(continuum_request)
        assert len(result["continuum_subband_integration_times"]) == 3

    @mock.patch("ska_ost_senscalc.mid.service.get_spectropolarimetry_results")
    def test_get_get_spectropolarimetry_results(
        self, mock_get_spectropolarimetry_results
    ):
        mock_get_spectropolarimetry_results.return_value = {
            "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
            "max_faraday_depth": Quantity(88, "rad / m^2"),
            "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
        }

        continuum_request = ContinuumRequest(
            freq_centre_hz=797500000.0,
            rx_band="Band 1",
            bandwidth_hz=435000000.0,
            pointing_centre=SkyCoord(
                "00:00:00.0 00:00:00.0", frame="icrs", unit=(hourangle, deg)
            ),
            weighting_mode=Weighting.UNIFORM,
            subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
            robustness=0.0,
            n_subbands=3,
            subband_freq_centres=[],
            spectral_averaging_factor=1.0,
            integration_time_s=600.0,
            supplied_sensitivity=None,
            sensitivity_unit="Jy/beam",
            n_ska=None,
            n_meer=None,
            pwv=10.0,
            el=45.0,
            taper=Quantity(0.0, u.arcsec),
            freq_centre=Quantity(7.975e08, u.Hz),
            telescope=Telescope.MID,
            subband_freq_centres_hz=[],
            subband_supplied_sensitivities=[],
            subband_supplied_sensitivities_unit="Jy/beam",
            target=None,
        )

        # result = get_continuum_calculate_response(params)
        result = convert_continuum_input_and_calculate(continuum_request)
        assert result["continuum_sensitivity"] > 0

        assert result["spectropolarimetry_results"] == {
            "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
            "max_faraday_depth": Quantity(88, "rad / m^2"),
            "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
        }

    # adding for sensitivity > integration time from TestContinuum  - need to reorder test_get_get_spectropolarimetry_results to the end
    def test_get_calculate_response_integration_time(self):
        continuum_request = ContinuumRequest(
            freq_centre_hz=797500000.0,
            rx_band="Band 1",
            bandwidth_hz=435000000.0,
            pointing_centre=SkyCoord(
                "00:00:00.0 00:00:00.0", frame="icrs", unit=(hourangle, deg)
            ),
            weighting_mode=Weighting.UNIFORM,
            subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
            robustness=0.0,
            n_subbands=1,
            subband_freq_centres=[],
            spectral_averaging_factor=1.0,
            integration_time_s=None,
            supplied_sensitivity=0.001,
            sensitivity_unit="Jy/beam",
            n_ska=None,
            n_meer=None,
            pwv=10.0,
            el=45.0,
            taper=Quantity(0.0, u.arcsec),
            freq_centre=Quantity(7.975e08, u.Hz),
            telescope=Telescope.MID,
            subband_freq_centres_hz=[],
            subband_supplied_sensitivities=[],
            subband_supplied_sensitivities_unit="Jy/beam",
            target=None,
        )

        result = convert_continuum_input_and_calculate(continuum_request)
        assert result["continuum_integration_time"] > 0
        assert result["spectral_integration_time"] > 0

    def test_get_calculate_response_zoom_integration_time(self):
        continuum_request = ContinuumRequest(
            freq_centre_hz=797500000.0,
            rx_band="Band 1",
            bandwidth_hz=435000000.0,
            pointing_centre=SkyCoord(
                "00:00:00.0 00:00:00.0", frame="icrs", unit=(hourangle, deg)
            ),
            weighting_mode=Weighting.UNIFORM,
            subarray_configuration=MIDArrayConfiguration.MID_AA4_ALL,
            robustness=0.0,
            n_subbands=1,
            subband_freq_centres=[],
            spectral_averaging_factor=1.0,
            integration_time_s=None,
            supplied_sensitivity=0.001,
            sensitivity_unit="Jy/beam",
            n_ska=None,
            n_meer=None,
            pwv=10.0,
            el=45.0,
            taper=Quantity(0.0, u.arcsec),
            freq_centre=Quantity(7.975e08, u.Hz),
            telescope=Telescope.MID,
            subband_freq_centres_hz=[],
            subband_supplied_sensitivities=[],
            subband_supplied_sensitivities_unit="Jy/beam",
            target=None,
        )
        result = convert_continuum_input_and_calculate(continuum_request)

        assert result["continuum_integration_time"] > 0
        assert "spectral_integration_time" in result.keys()
        assert result["spectral_integration_time"] > 0


class TestZoom:
    def test_get_calculate_response_zooms(self):
        pointing_centre = SkyCoord(
            "13:25:27.60 -43:01:09.00",
            frame="icrs",
            unit=(u.hourangle, u.deg),
        )
        params = {
            "telescope": Telescope.MID,
            "rx_band": "Band 5a",
            "total_bandwidths_hz": [400000000, 500000000],
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL,
            "pointing_centre": pointing_centre,
            "freq_centres_hz": [6400000000, 6600000000],
            "freq_centres": [Quantity(6400000000, u.Hz), Quantity(6600000000, u.Hz)],
            "spectral_resolutions_hz": [1000, 1000],
            "supplied_sensitivities": [1.0, 0.001],
            "taper": 0 * u.arcsec,
        }
        params = ZoomRequestPrepared(**params)
        result = get_zoom_calculate_response(params)
        assert len(result["calculate"]) == 2

    @mock.patch("ska_ost_senscalc.mid.service.get_spectropolarimetry_results")
    def test_get_get_spectropolarimetry_results(
        slef, mock_get_spectropolarimetry_results
    ):
        mock_get_spectropolarimetry_results.return_value = {
            "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
            "max_faraday_depth": Quantity(88, "rad / m^2"),
            "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
        }

        pointing_centre = SkyCoord(
            "13:25:27.60 -43:01:09.00",
            frame="icrs",
            unit=(u.hourangle, u.deg),
        )
        params = {
            "telescope": Telescope.MID,
            "freq_centres_hz": [6400000000, 6600000000],
            "freq_centres": [Quantity(6400000000, u.Hz), Quantity(6600000000, u.Hz)],
            "spectral_resolutions_hz": [1000, 1000],
            "supplied_sensitivities": [1.0, 0.001],
            "rx_band": "Band 5a",
            "total_bandwidths_hz": [400000000, 50000000],
            "subarray_configuration": MIDArrayConfiguration.MID_AA4_ALL,
            "pointing_centre": pointing_centre,
            "taper": 0 * u.arcsec,
        }
        params = ZoomRequestPrepared(**params)
        result = get_zoom_calculate_response(params)

        assert result["calculate"][0]["spectropolarimetry_results"] == {
            "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
            "max_faraday_depth": Quantity(88, "rad / m^2"),
            "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
        }
