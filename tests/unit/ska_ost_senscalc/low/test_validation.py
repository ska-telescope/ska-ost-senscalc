import astropy.units as u
import pytest
from astropy.coordinates import Latitude

from ska_ost_senscalc.common.model import (
    ContinuumCalculatorAndWeightingInput,
    LOWArrayConfiguration,
    Telescope,
    Weighting,
    WeightingSpectralMode,
    ZoomCalculatorAndWeightingInput,
    ZoomWeightingRequestParams,
)
from ska_ost_senscalc.low.validation import (
    PSS_BANDWIDTH_MHZ,
    _validate_pointing_centre,
    validate_and_convert_zoom_weighting_params,
    validate_and_set_defaults_for_continuum,
    validate_and_set_defaults_for_pss,
    validate_and_set_defaults_for_zoom,
)
from tests import LowErrorMessages as err_mess


class TestContinuum:
    def test_defaults_are_set(self):
        user_input = {"num_stations": 512, "weighting_mode": "uniform"}

        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="10:00:00 -30:00:00"), []
        )
        expected_with_defaults = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(2e08, u.Hz),
            bandwidth_mhz=300,
            num_stations=512.0,
            pointing_centre=py_pointing_centre,
            integration_time_h=1,
            elevation_limit=20,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
            spectral_averaging_factor=1,
            n_subbands=1,
            weighting_mode=Weighting.UNIFORM,
            robustness=0,
            subband_freq_centres=[],
            taper=u.Quantity(0.0, u.arcsec),
        )

        assert expected_with_defaults == validate_and_set_defaults_for_continuum(
            user_input
        )
        # Return a new dict rather than mutating
        assert {"num_stations": 512, "weighting_mode": "uniform"} == user_input

    @pytest.mark.parametrize(
        "user_input",
        [
            {
                "subarray_configuration": "LOW_AAstar_all",
                "num_stations": 200,
                "weighting_mode": "uniform",
            },
            {
                "subarray_configuration": "LOW_AAstar_all",
                "num_stations": 512,
                "weighting_mode": "uniform",
            },
        ],
    )
    def test_array_configuration_with_num_stations_raises_error(self, user_input):
        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.SUBARRAY_OR_NUM_STATIONS,)

    def test_spectral_window_within_limits(self):
        user_input = {
            "subarray_configuration": "LOW_AA4_all",
            "freq_centre_mhz": 100,
            "bandwidth_mhz": 50,
            "weighting_mode": "uniform",
        }

        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="10:00:00 -30:00:00"), []
        )
        expected_output = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(1e08, u.Hz),
            bandwidth_mhz=50,
            num_stations=512.0,
            pointing_centre=py_pointing_centre,
            integration_time_h=1,
            elevation_limit=20,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
            spectral_averaging_factor=1,
            n_subbands=1,
            weighting_mode=Weighting.UNIFORM,
            robustness=0,
            subband_freq_centres=[],
            taper=u.Quantity(0.0, u.arcsec),
        )

        assert expected_output == validate_and_set_defaults_for_continuum(user_input)

    def test_bandwidth_below_maximum_for_array_configuration(self):
        user_input = {
            "subarray_configuration": "LOW_AA05_all",
            "bandwidth_mhz": 100,
            "weighting_mode": "uniform",
        }

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.MAX_BANDWIDTH_FOR_SUBARRAY(75),)

    def test_spectral_window_below_minimum(self):
        user_input = {
            "num_stations": 512,
            "freq_centre_mhz": 100,
            "bandwidth_mhz": 150,
            "weighting_mode": "uniform",
        }

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.ZOOM_SPECTRAL_WINDOW,)

    def test_spectral_window_above_maximum(self):
        user_input = {
            "num_stations": 512,
            "freq_centre_mhz": 300,
            "bandwidth_mhz": 150,
            "weighting_mode": "uniform",
        }

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.ZOOM_SPECTRAL_WINDOW,)

    def test_invalid_pointing(self):
        user_input = {
            "num_stations": 512,
            "pointing_centre": "not a pointing",
            "weighting_mode": "uniform",
        }

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.INVALID_POINTING_CENTRE,)

    def test_invalid_spectral_averaging_factor(self):
        user_input = {
            "num_stations": 512,
            "spectral_averaging_factor": -1,
            "weighting_mode": "uniform",
        }

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.INVALID_SPECTRAL_AVERAGING_FACTOR(27_648),)

    def test_continuum_params_happy_path(self):
        user_input = {
            "weighting_mode": "uniform",
            "freq_centre_mhz": 200,
            "subband_freq_centres_mhz": [135],
            "subarray_configuration": "LOW_AA4_all",
            "pointing_centre": "13:25:27.60 -43:01:09.00",
        }

        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="13:25:27.60 -43:01:09.00"), []
        )
        expected = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(200e6, u.Hz),
            bandwidth_mhz=300,
            num_stations=512.0,
            pointing_centre=py_pointing_centre,
            integration_time_h=1,
            elevation_limit=20,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
            spectral_averaging_factor=1,
            n_subbands=1,
            weighting_mode=Weighting.UNIFORM,
            robustness=0,
            subband_freq_centres=[135e6 * u.Hz],
            taper=u.Quantity(0.0, u.arcsec),
        )

        assert expected == validate_and_set_defaults_for_continuum(user_input)

    def test_bad_enum_value(self):
        q = {
            "freq_centre_mhz": 650e6,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "subarray_configuration": "LOW_AA1_all",
            "weighting_mode": "robust",
            "robustness": 1,
        }
        q["subarray_configuration"] = "foo"
        with pytest.raises(ValueError):
            validate_and_set_defaults_for_continuum(q)

        q["weighting_mode"] = "foo"
        with pytest.raises(ValueError):
            validate_and_set_defaults_for_continuum(q)

    def test_robust_mode_but_robustness_not_provided(self):
        user_input = {
            "weighting_mode": "robust",
            "freq_centre_mhz": 650,
            "subarray_configuration": "LOW_AA1_all",
            "pointing_centre": "10:00:00.0 -30:00:00.0",
        }
        with pytest.raises(ValueError):
            validate_and_set_defaults_for_continuum(user_input)

    @pytest.mark.parametrize(
        "user_input",
        [
            {
                "num_stations": 512,
                "pointing_centre": "10:00:00.0 -30:00:00.0",
                "weighting_mode": "uniform",
                "n_subbands": 2,
                "subband_freq_centres_mhz": [150, 120, 100],
            },
            {
                "num_stations": 512,
                "pointing_centre": "10:00:00.0 -30:00:00.0",
                "weighting_mode": "uniform",
                "subband_freq_centres_mhz": [120, 100],
            },
            {
                "num_stations": 512,
                "pointing_centre": "10:00:00.0 -30:00:00.0",
                "weighting_mode": "uniform",
                "n_subbands": 2,
                "subband_freq_centres_mhz": [120],
            },
            {
                "num_stations": 150,
                "pointing_centre": "10:00:00.0 -30:00:00.0",
                "weighting_mode": "uniform",
                "n_subbands": 4,
                "subband_freq_centres_mhz": [150, 120, 100],
            },
        ],
    )
    def test_n_subband_not_matching_subband_freq_center_number(self, user_input):
        """
        If subband frequency center is provided in user input, it should match n_subbands

        so that the same number of subbands are generated for continuum_subband_sensitivities

        and continuum_weighting subbands.

        """
        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_continuum(user_input)

        assert err.value.args == (err_mess.N_SUBBANDS_FOR_SUBBAND_FREQ_CENTRES,)


class TestWeightingValidation:
    def test_weighting_zoom_params_happy_path(self):
        user_input = {
            "weighting_mode": "uniform",
            "freq_centres_mhz": [1.23],
            "subarray_configuration": "LOW_AA05_all",
            "pointing_centre": "13:25:27.60 -43:01:09.00",
        }

        expected = ZoomWeightingRequestParams(
            telescope=Telescope.LOW,
            freq_centres=[1.23e6 * u.Hz],
            dec=Latitude("-43:01:09.00", "deg"),
            subarray_configuration=LOWArrayConfiguration.LOW_AA05_ALL,
            weighting_mode=Weighting.UNIFORM,
            robustness=0,
            taper=0 * u.arcsec,
        )

        assert expected == validate_and_convert_zoom_weighting_params(**user_input)


class TestPSS:
    def test_defaults_are_set_folded(self):
        user_input = {"num_stations": 512, "pulsar_mode": "folded_pulse"}

        expected_defaults = {
            "num_stations": 512,
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 118.518513664,
            "spectral_resolution_hz": 14467.592,
            "pointing_centre": "10:00:00 -30:00:00",
            "elevation_limit": 20,
            "integration_time_h": 1,
            "dm": 0.0,
            "pulse_period": 33,
            "intrinsic_pulse_width": 0.004,
            "pulsar_mode": "folded_pulse",
        }

        assert expected_defaults == validate_and_set_defaults_for_pss(user_input)

    def test_defaults_are_set_single(self):
        user_input = {"num_stations": 512, "pulsar_mode": "single_pulse"}

        expected_defaults = {
            "num_stations": 512,
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 118.518513664,
            "spectral_resolution_hz": 14467.592,
            "pointing_centre": "10:00:00 -30:00:00",
            "elevation_limit": 20,
            "integration_time_h": 1,
            "dm": 0.0,
            "pulse_period": 33,
            "intrinsic_pulse_width": 0.004,
            "pulsar_mode": "single_pulse",
        }

        assert expected_defaults == validate_and_set_defaults_for_pss(user_input)

    def test_bandwidth_not_overwritten_single(self):
        """
        Test to ensure that the user-specified bandwidth is preserved.
        """
        user_input = {
            "num_stations": 512,
            "bandwidth_mhz": 1,
            "pulsar_mode": "single_pulse",
        }

        expected_defaults = {
            "num_stations": 512,
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 1,
            "spectral_resolution_hz": 14467.592,
            "pointing_centre": "10:00:00 -30:00:00",
            "elevation_limit": 20,
            "integration_time_h": 1,
            "dm": 0.0,
            "pulse_period": 33,
            "intrinsic_pulse_width": 0.004,
            "pulsar_mode": "single_pulse",
        }

        assert expected_defaults == validate_and_set_defaults_for_pss(user_input)

    def test_bandwidth_overwritten_folded(self):
        """
        In the folded-pulse mode, bandwidth, if specified by the user, should be ignored
         and overwritten by the default value.
        """
        user_input = {
            "num_stations": 512,
            "bandwidth_mhz": 1,
            "pulsar_mode": "folded_pulse",
        }

        expected_defaults = {
            "num_stations": 512,
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 118.518513664,
            "spectral_resolution_hz": 14467.592,
            "pointing_centre": "10:00:00 -30:00:00",
            "elevation_limit": 20,
            "integration_time_h": 1,
            "dm": 0.0,
            "pulse_period": 33,
            "intrinsic_pulse_width": 0.004,
            "pulsar_mode": "folded_pulse",
        }

        assert expected_defaults == validate_and_set_defaults_for_pss(user_input)

    def test_invalid_pulse_width_folded(self):
        user_input = {
            "num_stations": 512,
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 118,
            "spectral_resolution_hz": 14467.592,
            "pointing_centre": "10:00:00 -30:00:00",
            "integration_time_h": 1,
            "dm": 0.0,
            "pulse_period": 33,
            "intrinsic_pulse_width": 33,
            "pulsar_mode": "folded_pulse",
        }

        expected_err = "Intrinsic pulse width cannot be larger than the pulse period."

        with pytest.raises(ValueError) as err:
            _ = validate_and_set_defaults_for_pss(user_input)

        assert expected_err in err.value.args

    def test_invalid_bandwidth_single(self):
        """
        In single-pulse mode, bandwidth cannot be larger than
        ska_ost_senscalc.low.validation.PSS_BANDWIDTH_MHZ
        """
        user_input = {
            "bandwidth_mhz": 119,
            "pulsar_mode": "single_pulse",
        }

        expected_err = (
            "For single-pulse calculations, bandwidth cannot be greater "
            f"than {PSS_BANDWIDTH_MHZ} MHz"
        )

        with pytest.raises(ValueError) as err:
            _ = validate_and_set_defaults_for_pss(user_input)

        assert expected_err in err.value.args

    @pytest.mark.parametrize("pulsar_mode", ["single_pulse", "folded_pulse"])
    def test_spectral_window_below_minimum(self, pulsar_mode):
        user_input = {
            "freq_centre_mhz": 100,
            "bandwidth_mhz": 118,
            "pulsar_mode": pulsar_mode,
        }

        expected_err = (
            "Spectral window defined by central frequency and bandwidth does"
            " not lie within the 50 - 350 MHz range."
        )

        with pytest.raises(ValueError) as err:
            _ = validate_and_set_defaults_for_pss(user_input)

        assert expected_err in err.value.args

    @pytest.mark.parametrize("pulsar_mode", ["single_pulse", "folded_pulse"])
    def test_spectral_window_above_maximum(self, pulsar_mode):
        user_input = {
            "freq_centre_mhz": 300,
            "bandwidth_mhz": 118,
            "pulsar_mode": pulsar_mode,
        }

        expected_err = (
            "Spectral window defined by central frequency and bandwidth does"
            " not lie within the 50 - 350 MHz range."
        )

        with pytest.raises(ValueError) as err:
            _ = validate_and_set_defaults_for_pss(user_input)

        assert expected_err in err.value.args

    @pytest.mark.parametrize("pulsar_mode", ["single_pulse", "folded_pulse"])
    def test_spectral_window_within_limits(self, pulsar_mode):
        user_input = {
            "freq_centre_mhz": 200,
            "num_stations": 512,
            "pulsar_mode": pulsar_mode,
        }

        expected_defaults = {
            "num_stations": 512,
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 118.518513664,
            "spectral_resolution_hz": 14467.592,
            "pointing_centre": "10:00:00 -30:00:00",
            "elevation_limit": 20,
            "integration_time_h": 1,
            "dm": 0.0,
            "pulse_period": 33,
            "intrinsic_pulse_width": 0.004,
            "pulsar_mode": pulsar_mode,
        }

        assert expected_defaults == validate_and_set_defaults_for_pss(user_input)

    @pytest.mark.parametrize(
        "user_input",
        [
            {
                "subarray_configuration": "LOW_AAstar_all",
                "num_stations": 200,
                "pulsar_mode": "single_pulse",
            },
        ],
    )
    def test_array_configuration_with_num_stations_raises_error(self, user_input):
        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_pss(user_input)

        assert err.value.args == (err_mess.SUBARRAY_OR_NUM_STATIONS,)


class TestZoom:
    WEIGHTING_INPUT = {
        "weighting_mode": "uniform",
        "robustness": 0.0,
    }

    def test_defaults_are_set(self):
        user_input = {"num_stations": 512}

        combined_input = user_input | self.WEIGHTING_INPUT

        expected = ZoomCalculatorAndWeightingInput(
            dec=Latitude(-30, "deg"),
            num_stations=512,
            integration_time_h=1,
            elevation_limit=20,
            spectral_averaging_factor=1,
            spectral_resolutions_hz=[14.1285],
            total_bandwidths_khz=[390.6],
            freq_centres=[u.Quantity(2e8, u.Hz)],
            pointing_centre=_validate_pointing_centre(
                dict(pointing_centre="10:00:00 -30:00:00"), []
            ),
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            taper=u.Quantity(0, u.arcsec),
            telescope=Telescope.LOW,
        )

        result = validate_and_set_defaults_for_zoom(combined_input)

        assert expected == result

    @pytest.mark.parametrize(
        "user_input",
        [{"subarray_configuration": "LOW_AAstar_all", "num_stations": 200}],
    )
    def test_array_configuration_with_num_stations_raises_error(self, user_input):
        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(self.WEIGHTING_INPUT | user_input)

        assert err.value.args == (err_mess.SUBARRAY_OR_NUM_STATIONS,)

    @pytest.mark.parametrize(
        "subarray_configuration",
        ["LOW_AAstar_core_only", "LOW_AAstar_all", "LOW_AA4_core_only", "LOW_AA4_all"],
    )
    def test_total_bandwidth_not_allowed_in_zoom_mode_with_full_subarrays(
        self, subarray_configuration
    ):
        """
        The total bandwidth can only be a certain set of values, given by the spectral resolution
        and the number of channels.

        This also depends on the subarray. For AA*, AA4 the full set of zoom modes are allowed

        Rather than match against an exact floating point number, we match to 0.1 kHz
        """
        user_input = {
            "subarray_configuration": subarray_configuration,
            "freq_centres_mhz": [100],
            "total_bandwidths_khz": [24],
        }

        combined_input = self.WEIGHTING_INPUT | user_input

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(combined_input)

        assert err.value.args == (
            err_mess.ZOOM_TOTAL_BANDWIDTH("24", subarray_configuration),
        )

    def test_total_bandwidth_not_allowed_in_zoom_mode_for_custom_subarray(self):
        """
        The total bandwidth can only be a certain set of values, given by the spectral resolution
        and the number of channels.

        This also depends on the subarray. For a custom subarray (ie where the user gives num_stations) the full set of zoom modes are allowed

        Rather than match against an exact floating point number, we match to 0.1 kHz
        """
        user_input = {
            "num_stations": 16,
            "freq_centres_mhz": [100],
            "total_bandwidths_khz": [24],
        }

        combined_input = self.WEIGHTING_INPUT | user_input

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(combined_input)

        assert err.value.args == (err_mess.ZOOM_TOTAL_BANDWIDTH("24", "custom"),)

    @pytest.mark.parametrize(
        "subarray_configuration",
        ["LOW_AA2_core_only", "LOW_AA2_all"],
    )
    def test_total_bandwidth_not_allowed_in_zoom_mode_with_aa2_subarrays(
        self, subarray_configuration
    ):
        """
        The total bandwidth can only be a certain set of values, given by the spectral resolution
        and the number of channels.

        This also depends on the subarray. For AA2 only a subset of the final 4 zoom modes are allowed

        Rather than match against an exact floating point number, we match to 0.1 kHz
        """
        user_input = {
            "subarray_configuration": subarray_configuration,
            "freq_centres_mhz": [100],
            "total_bandwidths_khz": [24],
        }

        combined_input = self.WEIGHTING_INPUT | user_input

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(combined_input)

        assert err.value.args == (
            err_mess.ZOOM_TOTAL_BANDWIDTH_SUBSET("24", subarray_configuration),
        )

    def test_spectral_window_below_minimum(self):
        user_input = {
            "num_stations": 512,
            "freq_centres_mhz": [51.5],
            "total_bandwidths_khz": [3125],
        }

        combined_input = self.WEIGHTING_INPUT | user_input

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(combined_input)

        assert err.value.args == (err_mess.ZOOM_SPECTRAL_WINDOW,)

    def test_spectral_window_above_maximum(self):
        user_input = {
            "num_stations": 512,
            "freq_centres_mhz": [348.5],
            "total_bandwidths_khz": [3125],
        }

        combined_input = self.WEIGHTING_INPUT | user_input

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(combined_input)

        assert err.value.args == (err_mess.ZOOM_SPECTRAL_WINDOW,)

    def test_invalid_pointing(self):
        user_input = {"num_stations": 512, "pointing_centre": "not a pointing"}

        combined_input = self.WEIGHTING_INPUT | user_input

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(combined_input)

        assert err.value.args == (err_mess.INVALID_POINTING_CENTRE,)

    @pytest.mark.parametrize(
        "user_input",
        [
            {  # 'spectral_resolutions_hz' not in input
                "total_bandwidths_khz": [1, 2],
                "freq_centres_mhz": [1, 2],
                "rx_band": "Band 1",
                "supplied_sensitivities": [1, 2],
                "subarray_configuration": "LOW_AA4_all",
            },
            {  # 'total_bandwidths_khz' not in input
                "spectral_resolutions_hz": [1, 2],
                "freq_centres_mhz": [1, 2],
                "rx_band": "Band 1",
                "bandwidth_hz": 0.1e9,
                "supplied_sensitivities": [1, 2],
                "subarray_configuration": "LOW_AA4_all",
            },
            {  # 'freq_centres_mhz' not in input
                "total_bandwidths_khz": [1, 2],
                "spectral_resolutions_hz": [1, 2],
                "rx_band": "Band 1",
                "bandwidth_hz": 0.1e9,
                "supplied_sensitivities": [1, 2],
                "subarray_configuration": "LOW_AA4_all",
            },
            {  # inputs are different lengths
                "total_bandwidths_khz": [1, 2, 3],
                "spectral_resolutions_hz": [1, 2],
                "freq_centres_mhz": [1, 2, 3, 4, 5],
                "rx_band": "Band 1",
                "bandwidth_hz": 0.1e9,
                "supplied_sensitivities": [1, 2],
                "subarray_configuration": "LOW_AA4_all",
            },
        ],
    )
    def test_spectral_resolutions_and_freq_centres_and_total_bandwidths_are_set_together(
        self, user_input
    ):
        combined_input = self.WEIGHTING_INPUT | user_input

        with pytest.raises(ValueError) as err:
            validate_and_set_defaults_for_zoom(combined_input)

        err_msg = "Parameters 'freq_centres_mhz', 'spectral_resolutions_hz' and 'total_bandwidths_khz' must all be set together and have the same length."
        assert err_msg in err.value.args[0].split(";")
