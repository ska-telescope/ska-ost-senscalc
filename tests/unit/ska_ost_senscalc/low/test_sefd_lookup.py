import numpy as np

from ska_ost_senscalc.low.sefd_lookup import SEFDTable


def test_sefd_lookup():
    """
    Test that SEFD lookup values return as expected
    """
    sefd_table = SEFDTable()

    # Zenith pointing, across 50-350 MHz
    freqs = np.arange(50, 350, 50)
    result = sefd_table.lookup_stokes_i_sefd(0, 90, 0, 1, freqs)
    gold = np.array(
        [
            9840.98973348,
            1727.15630602,
            1340.52268411,
            1506.31902387,
            1579.43982495,
            1754.73719598,
        ]
    )
    assert np.allclose(result, gold)

    # 45 degree az + el pointing, 100-200 MHz
    freqs = np.arange(100, 201, 25)
    result = sefd_table.lookup_stokes_i_sefd(45, 45, 15, 19, freqs)
    gold = np.array(
        [8966.77900267, 8181.97775467, 8925.19735868, 6691.84579349, 5901.9087032]
    )
    assert np.allclose(result, gold)
