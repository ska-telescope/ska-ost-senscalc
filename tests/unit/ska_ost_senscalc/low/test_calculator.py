import numpy as np
import pytest

from ska_ost_senscalc.low.calculator import _stack_sensitivities, calculate_sensitivity
from ska_ost_senscalc.low.model import CalculatorInput, CalculatorResult
from tests import LowErrorMessages as err_mess


def isclose(x: CalculatorResult, y: CalculatorResult) -> bool:
    """Check two results are close (equivalent within Numpy's tolerance)"""
    return np.isclose(x._to_quantity(), y._to_quantity(), rtol=1e-4)


def test_calculator_duration_fits_in_visible_time():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre_mhz=200,
        bandwidth_mhz=300,
        pointing_centre="10:00:00 -30:00:00",
        integration_time_h=0.05,
        elevation_limit=20,
    )
    assert isclose(
        calculate_sensitivity(calculator_input),
        CalculatorResult(21.880681504687406, "uJy/beam"),
    )


def test_calculator_duration_longer_than_visible_time():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre_mhz=200,
        bandwidth_mhz=300,
        pointing_centre="10:00:00 -30:00:00",
        integration_time_h=10,
        elevation_limit=20,
    )
    assert isclose(
        calculate_sensitivity(calculator_input),
        CalculatorResult(2.1702, "uJy/beam"),
    )


def test_calculator_source_always_up():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre_mhz=200,
        bandwidth_mhz=300,
        pointing_centre="00:00:00 -80:00:00",
        integration_time_h=0.05,
        elevation_limit=20,
    )
    assert isclose(
        calculate_sensitivity(calculator_input),
        CalculatorResult(101.67073211215421, "uJy/beam"),
    )


def test_calculator_btn_2122():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre_mhz=200,
        bandwidth_mhz=300,
        pointing_centre="00:00:00 -30:00:00",
        integration_time_h=1.83,
        elevation_limit=20,
    )
    assert isclose(
        calculate_sensitivity(calculator_input),
        CalculatorResult(3.2804, "uJy/beam"),
    )

    calculator_input2 = CalculatorInput(
        num_stations=512,
        freq_centre_mhz=200,
        bandwidth_mhz=300,
        pointing_centre="00:00:00 -30:00:00",
        integration_time_h=1.84,
        elevation_limit=20,
    )
    assert isclose(
        calculate_sensitivity(calculator_input2),
        CalculatorResult(3.2709, "uJy/beam"),
    )


def test_calculator_source_never_up_raises_error():
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre_mhz=200,
        bandwidth_mhz=300,
        pointing_centre="00:00:00 80:00:00",
        integration_time_h=0.05,
        elevation_limit=20,
    )
    with pytest.raises(ValueError) as err:
        calculate_sensitivity(calculator_input)

    assert err.value.args[0] == err_mess.ALWAYS_BELOW_HORIZON


def test_calculator_source_never_up_raises_error_for_low_elevation():
    """
    The sensitivities can't be calculated for low elevation, so we set the horizon of the observer
    object to be 15 degrees.
    """
    calculator_input = CalculatorInput(
        num_stations=512,
        freq_centre_mhz=200,
        bandwidth_mhz=300,
        pointing_centre="08:00:00 50:00:00",
        integration_time_h=0.05,
        elevation_limit=20,
    )
    with pytest.raises(ValueError) as err:
        calculate_sensitivity(calculator_input)

    assert err.value.args[0] == err_mess.ALWAYS_BELOW_HORIZON


def test_calculator_result():
    """Exercise all the dunder comparisons on CalculatorResult"""
    r1 = CalculatorResult(1, "uJy/beam")
    r2 = CalculatorResult(2, "uJy/beam")
    assert r1 == r1
    assert r1 != r2
    assert r1 < r2
    assert r1 <= r2
    assert r2 > r1
    assert r2 >= r1


def test_stack_sensitivities_weighted():
    """Test that sensitivities are weighted when adding them together"""
    upper_bound = _stack_sensitivities([1, 1, 1], n_scans=3)
    lower_bound = _stack_sensitivities([1, 1, 1, 1], n_scans=4)
    calc_noisy_int = _stack_sensitivities([1, 1, 1, 10], n_scans=4)

    assert lower_bound < calc_noisy_int < upper_bound
