from unittest import mock

from astropy import units as u
from astropy.coordinates import Latitude
from astropy.units import Quantity

from ska_ost_senscalc.common.model import (
    ContinuumCalculatorAndWeightingInput,
    Weighting,
    WeightingSpectralMode,
    ZoomCalculatorAndWeightingInput,
)
from ska_ost_senscalc.low.model import CalculatorInput, CalculatorResult
from ska_ost_senscalc.low.service import (
    convert_continuum_input_and_calculate,
    convert_pss_input_and_calculate,
    convert_zoom_input_and_calculate,
)
from ska_ost_senscalc.low.validation import (
    _validate_pointing_centre,
    validate_and_set_defaults_for_continuum,
)
from ska_ost_senscalc.subarray import LOWArrayConfiguration
from ska_ost_senscalc.utilities import Telescope


class TestContinuum:
    @mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
    @mock.patch(
        "ska_ost_senscalc.low.bright_source_lookup.BrightSourceCatalog.check_for_bright_sources"
    )
    @mock.patch("ska_ost_senscalc.low.service.get_spectropolarimetry_results")
    def test_calls_calculator_with_converted_input(
        self,
        mock_get_spectropolarimetry_results,
        mock_bright_src_check,
        mock_calculate_fn,
    ):
        mock_calculate_fn.return_value = CalculatorResult(
            sensitivity=123, units="uJy/beam"
        )
        mock_bright_src_check.return_value = True
        mock_get_spectropolarimetry_results.return_value = {
            "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
            "max_faraday_depth": Quantity(88, "rad / m^2"),
            "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
        }

        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="13:25:28 -43:01:09"), []
        )
        validated_and_updated_params = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(4.0e08, u.Hz),
            bandwidth_mhz=125.0,
            num_stations=42,  # custom array
            pointing_centre=py_pointing_centre,
            integration_time_h=0.05,
            elevation_limit=20.0,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=None,
            spectral_averaging_factor=1.0,
            n_subbands=1,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            subband_freq_centres=None,
            taper=u.Quantity(0, u.arcsec),
        )

        expected_calculator_input1 = CalculatorInput(
            freq_centre_mhz=400.0,
            bandwidth_mhz=125.0,
            num_stations=42,
            pointing_centre=py_pointing_centre,
            integration_time_h=0.05,
            elevation_limit=20.0,
        )

        expected_calculator_input2 = CalculatorInput(
            freq_centre_mhz=400.0,
            bandwidth_mhz=0.005425347222222222,
            num_stations=42,
            pointing_centre=py_pointing_centre,
            integration_time_h=0.05,
            elevation_limit=20.0,
        )
        expected_calls = (
            mock.call(expected_calculator_input1),
            mock.call(expected_calculator_input2),
        )

        expected_result = {
            "calculate": {
                "continuum_sensitivity": Quantity(123, "uJy/beam"),
                "spectral_sensitivity": Quantity(123, "uJy/beam"),
                "spectropolarimetry_results": {
                    "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
                    "max_faraday_depth": Quantity(88, "rad / m^2"),
                    "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
                },
                "continuum_subband_sensitivities": [],
                "warnings": [
                    "The specified pointing contains at least one source brighter than"
                    " 10.0 Jy. Your observation may be dynamic range limited."
                ],
            },
            "weighted_result": {
                "weighted_continuum_sensitivity": Quantity(
                    0.00012299999999999998, "Jy / beam"
                ),
                "continuum_confusion_noise": None,
                "total_continuum_sensitivity": None,
                "continuum_synthesized_beam_size": None,
                "continuum_surface_brightness_sensitivity": None,
                "weighted_spectral_sensitivity": Quantity(
                    0.00012299999999999998, "Jy / beam"
                ),
                "spectral_confusion_noise": None,
                "total_spectral_sensitivity": None,
                "spectral_synthesized_beam_size": None,
                "spectral_surface_brightness_sensitivity": None,
                "confusion_noise_per_subband": None,
                "continuum_integration_time": None,
                "integration_time_per_subband": None,
                "spectral_integration_time": None,
                "synthesized_beam_size_per_subband": None,
                "warnings": [],
            },
            "weighting": {"continuum_weighting": None, "spectral_weighting": None},
        }
        result = convert_continuum_input_and_calculate(validated_and_updated_params)
        assert result["weighted_result"] == expected_result["weighted_result"]
        mock_calculate_fn.assert_has_calls(expected_calls)

    @mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
    @mock.patch(
        "ska_ost_senscalc.low.bright_source_lookup.BrightSourceCatalog.check_for_bright_sources"
    )
    @mock.patch("ska_ost_senscalc.low.service.get_spectropolarimetry_results")
    def test_convert_input_and_calculate_no_bright_source(
        self,
        mock_get_spectropolarimetry_results,
        mock_bright_src_check,
        mock_calculate_fn,
    ):
        """
        ska_ost_senscalc.low.service.convert_continuum_input_and_calculate() should not issue
        a warning message when the specified pointing does not contain a bright source.
        """
        mock_calculate_fn.return_value = CalculatorResult(123, "uJy/beam")
        mock_bright_src_check.return_value = False
        mock_get_spectropolarimetry_results.return_value = {
            "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
            "max_faraday_depth": Quantity(88, "rad / m^2"),
            "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
        }

        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="03:30:00 -30:00:00"), []
        )
        validated_and_updated_params = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(2e08, u.Hz),
            bandwidth_mhz=300.0,
            num_stations=512,
            pointing_centre=py_pointing_centre,
            integration_time_h=10.0,
            elevation_limit=20.0,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
            spectral_averaging_factor=1.0,
            n_subbands=1,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            subband_freq_centres=None,
            taper=u.Quantity(0, u.arcsec),
        )

        expected_result = {
            "continuum_sensitivity": Quantity(123, "uJy/beam"),
            "spectral_sensitivity": Quantity(123, "uJy/beam"),
            "continuum_subband_sensitivities": [],
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
                "max_faraday_depth": Quantity(88, "rad / m^2"),
                "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
            },
            "warnings": [],
        }

        result = convert_continuum_input_and_calculate(validated_and_updated_params)
        assert (
            result["calculate"]["continuum_sensitivity"]
            == expected_result["continuum_sensitivity"]
        )
        assert (
            result["calculate"]["spectral_sensitivity"]
            == expected_result["spectral_sensitivity"]
        )
        assert (
            result["calculate"]["continuum_subband_sensitivities"]
            == expected_result["continuum_subband_sensitivities"]
        )
        assert (
            result["calculate"]["spectropolarimetry_results"]
            == expected_result["spectropolarimetry_results"]
        )
        assert result["calculate"]["warnings"] == expected_result["warnings"]

    @mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
    @mock.patch(
        "ska_ost_senscalc.low.bright_source_lookup.BrightSourceCatalog.check_for_bright_sources"
    )
    @mock.patch("ska_ost_senscalc.low.service.get_spectropolarimetry_results")
    def test_convert_input_and_calculate_with_subbands(
        self,
        mock_get_spectropolarimetry_results,
        mock_bright_src_check,
        mock_calculate_fn,
    ):
        """
        ska_ost_senscalc.low.service.convert_continuum_input_and_calculate() should not issue
        a warning message when the specified pointing does not contain a bright source.
        """
        mock_calculate_fn.return_value = CalculatorResult(123, "uJy/beam")
        mock_bright_src_check.return_value = False
        mock_get_spectropolarimetry_results.return_value = {
            "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
            "max_faraday_depth": Quantity(88, "rad / m^2"),
            "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
        }

        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="03:30:00 -30:00:00"), []
        )
        validated_and_updated_params = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(2e08, u.Hz),
            bandwidth_mhz=300.0,
            num_stations=512,
            pointing_centre=py_pointing_centre,
            integration_time_h=10.0,
            elevation_limit=20.0,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
            spectral_averaging_factor=1.0,
            n_subbands=2,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            subband_freq_centres=None,
            taper=u.Quantity(0, u.arcsec),
        )

        expected_result = {
            "continuum_sensitivity": Quantity(123, "uJy/beam"),
            "spectral_sensitivity": Quantity(123, "uJy/beam"),
            "continuum_subband_sensitivities": [
                {
                    "subband_freq_centre": Quantity(125.0, "MHz"),
                    "sensitivity": Quantity(123, "uJy / beam"),
                },
                {
                    "subband_freq_centre": Quantity(275, "MHz"),
                    "sensitivity": Quantity(123, "uJy / beam"),
                },
            ],
            "spectropolarimetry_results": {
                "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
                "max_faraday_depth": Quantity(88, "rad / m^2"),
                "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
            },
            "warnings": [],
        }

        result = convert_continuum_input_and_calculate(validated_and_updated_params)
        assert (
            result["calculate"]["continuum_sensitivity"]
            == expected_result["continuum_sensitivity"]
        )
        assert (
            result["calculate"]["spectral_sensitivity"]
            == expected_result["spectral_sensitivity"]
        )
        assert (
            result["calculate"]["continuum_subband_sensitivities"]
            == expected_result["continuum_subband_sensitivities"]
        )
        assert (
            result["calculate"]["spectropolarimetry_results"]
            == expected_result["spectropolarimetry_results"]
        )
        assert result["calculate"]["warnings"] == expected_result["warnings"]

    def test_sensitivity_elevation_limit(self):
        """
        Test to verify that sensitivity goes down with increasing elevation limit.

        TODO move this - it is a slow test and really it is testing the calculator rather than the service
        """
        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="03:30:00 -30:00:00"), []
        )
        validated_and_updated_params = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(2e08, u.Hz),
            bandwidth_mhz=300.0,
            num_stations=512,
            pointing_centre=py_pointing_centre,
            integration_time_h=10.0,
            elevation_limit=20.0,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
            spectral_averaging_factor=1.0,
            n_subbands=2,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            subband_freq_centres=None,
            taper=u.Quantity(0, u.arcsec),
        )

        sensitivity_20deg = convert_continuum_input_and_calculate(
            validated_and_updated_params
        )
        validated_and_updated_params.elevation_limit = 40
        sensitivity_40deg = convert_continuum_input_and_calculate(
            validated_and_updated_params
        )

        assert (
            sensitivity_20deg["calculate"]["continuum_sensitivity"].value
            > sensitivity_40deg["calculate"]["continuum_sensitivity"].value
        )

    @mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
    def test_num_stations_set_from_subarray_configuration(self, mock_calculate_fn):
        """
        num_stations should be derived from the subarray_configuration
        """
        mock_calculate_fn.return_value = CalculatorResult(30, "uJy/beam")

        user_input = {
            "subarray_configuration": "LOW_AA4_all",
            "freq_centre_mhz": 400,
            "bandwidth_mhz": 100,
            "pointing_centre": "13:25:28 -43:01:09",
            "integration_time_h": 0.05,
            "elevation_limit": 20,
            "spectral_averaging_factor": 1,
        }
        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="13:25:28 -43:01:09"), []
        )
        validated_and_updated_params = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(4e08, u.Hz),
            bandwidth_mhz=100.0,
            num_stations=512,
            pointing_centre=py_pointing_centre,
            integration_time_h=0.05,
            elevation_limit=20.0,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
            spectral_averaging_factor=1.0,
            n_subbands=1,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            subband_freq_centres=None,
            taper=u.Quantity(0, u.arcsec),
        )

        expected_calculator_input1 = CalculatorInput(
            freq_centre_mhz=400,
            bandwidth_mhz=100,
            num_stations=512,  # Assert that this param is set
            pointing_centre=py_pointing_centre,
            integration_time_h=0.05,
            elevation_limit=20,
        )

        expected_calculator_input2 = CalculatorInput(
            freq_centre_mhz=400,
            bandwidth_mhz=0.005425347222222222,
            num_stations=512,  # Assert that this param is set
            pointing_centre=py_pointing_centre,
            integration_time_h=0.05,
            elevation_limit=20,
        )

        expected_calls = (
            mock.call(expected_calculator_input1),
            mock.call(expected_calculator_input2),
        )

        convert_continuum_input_and_calculate(validated_and_updated_params)

        mock_calculate_fn.assert_has_calls(expected_calls)

    def test_subarray_configuration_set_from_num_stations(self):
        """
        the subarray_configuration should be derived from num_stations
        """

        user_input = {
            "num_stations": 512,
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 300,
            "pointing_centre": "13:25:28 -43:01:09",
            "integration_time_h": 0.05,
            "elevation_limit": 20,
            "spectral_averaging_factor": 1,
            "weighting_mode": "uniform",
        }

        expected_validated_and_updated_params = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(2e08, u.Hz),
            bandwidth_mhz=300.0,
            num_stations=512,
            pointing_centre=_validate_pointing_centre(
                dict(pointing_centre="13:25:28 -43:01:09"), []
            ),
            integration_time_h=0.05,
            elevation_limit=20.0,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,  # Assert that this param is set
            spectral_averaging_factor=1.0,
            n_subbands=1,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            subband_freq_centres=[],
            taper=u.Quantity(0, u.arcsec),
        )

        validated_and_updated_params = validate_and_set_defaults_for_continuum(
            user_input
        )

        assert validated_and_updated_params == expected_validated_and_updated_params

    def test_subarray_configuration_set_to_None_for_custom_array(self):
        """
        the subarray_configuration should be set to None for custom subarray_configuration
        """

        user_input = {
            "num_stations": 100,  # this is a custom conf
            "freq_centre_mhz": 200,
            "bandwidth_mhz": 300,
            "pointing_centre": "13:25:28 -43:01:09",
            "integration_time_h": 0.05,
            "elevation_limit": 20,
            "spectral_averaging_factor": 1,
            "weighting_mode": "uniform",
        }

        expected_validated_and_updated_params = ContinuumCalculatorAndWeightingInput(
            freq_centre=u.Quantity(2e08, u.Hz),
            bandwidth_mhz=300.0,
            num_stations=100,
            pointing_centre=_validate_pointing_centre(
                dict(pointing_centre="13:25:28 -43:01:09"), []
            ),
            integration_time_h=0.05,
            elevation_limit=20.0,
            telescope=Telescope.LOW,
            spectral_mode=WeightingSpectralMode.CONTINUUM,
            subarray_configuration=None,  # Assert that this param is set
            spectral_averaging_factor=1.0,
            n_subbands=1,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            subband_freq_centres=[],
            taper=u.Quantity(0, u.arcsec),
        )

        validated_and_updated_params = validate_and_set_defaults_for_continuum(
            user_input
        )

        assert validated_and_updated_params == expected_validated_and_updated_params


class TestZoom:
    @mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
    @mock.patch(
        "ska_ost_senscalc.low.bright_source_lookup.BrightSourceCatalog.check_for_bright_sources"
    )
    @mock.patch("ska_ost_senscalc.low.service.get_spectropolarimetry_results")
    def test_calls_calculator_with_converted_input(
        self,
        mock_get_spectropolarimetry_results,
        mock_bright_src_check,
        mock_calculate_fn,
    ):
        """
        This is mainly testing that the spectral resolution from the request is used as the bandwidth in the calculation
        """
        mock_calculate_fn.return_value = CalculatorResult(123, "uJy/beam")
        mock_bright_src_check.return_value = True
        mock_get_spectropolarimetry_results.return_value = {
            "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
            "max_faraday_depth": Quantity(88, "rad / m^2"),
            "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
        }

        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="13:25:28 -43:01:09"), []
        )

        input_dataclass = ZoomCalculatorAndWeightingInput(
            dec=Latitude(-43.01916667, u.deg),
            num_stations=42,
            integration_time_h=0.05,
            elevation_limit=20,
            spectral_resolutions_hz=[1808.4],
            total_bandwidths_khz=[3125],
            freq_centres=[u.Quantity(4.0e08, u.Hz)],
            pointing_centre=py_pointing_centre,
            subarray_configuration=None,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            taper=Quantity(0, u.arcsec),
            telescope=Telescope.LOW,
        )

        expected_calculator_input = CalculatorInput(
            freq_centre_mhz=400,
            bandwidth_mhz=0.0018084,
            num_stations=42,
            pointing_centre=py_pointing_centre,
            integration_time_h=0.05,
            elevation_limit=20,
        )

        result = convert_zoom_input_and_calculate(input_dataclass)

        expected = [
            {
                "freq_centre": Quantity(400, "MHz"),
                "spectral_sensitivity": Quantity(123, "uJy/beam"),
                "spectropolarimetry_results": {
                    "fwhm_of_the_rmsf": Quantity(99, "rad / m^2"),
                    "max_faraday_depth": Quantity(88, "rad / m^2"),
                    "max_faraday_depth_extent": Quantity(77, "rad / m^2"),
                },
                "warnings": [
                    "The specified pointing contains at least one source brighter than"
                    " 10.0 Jy. Your observation may be dynamic range limited."
                ],
            }
        ]
        assert result["calculate"] == expected

        mock_calculate_fn.assert_called_once_with(expected_calculator_input)

    @mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
    def test_num_stations_set_from_subarray_configuration(self, mock_calculate_fn):
        """
        num_stations should be derived from the subarray_configuration
        """
        mock_calculate_fn.return_value = CalculatorResult(123, "uJy/beam")
        py_pointing_centre = _validate_pointing_centre(
            dict(pointing_centre="13:25:28 -43:01:09"), []
        )

        expected_calculator_input = CalculatorInput(
            freq_centre_mhz=400,
            bandwidth_mhz=0.0018084,
            num_stations=512,  # Assert that this param is set
            pointing_centre=py_pointing_centre,
            integration_time_h=0.05,
            elevation_limit=20,
        )

        input_dataclass = ZoomCalculatorAndWeightingInput(
            dec=Latitude(-43.01916667, u.deg),
            num_stations=512,
            integration_time_h=0.05,
            elevation_limit=20,
            spectral_resolutions_hz=[1808.4],
            total_bandwidths_khz=[3125],
            freq_centres=[u.Quantity(4.0e08, u.Hz)],
            pointing_centre=py_pointing_centre,
            subarray_configuration=None,
            weighting_mode=Weighting.UNIFORM,
            robustness=0.0,
            taper=Quantity(0, u.arcsec),
            telescope=Telescope.LOW,
        )

        convert_zoom_input_and_calculate(input_dataclass)

        mock_calculate_fn.assert_called_once_with(expected_calculator_input)


class TestPSS:
    @mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
    def test_convert_input_and_calculate_single(self, mock_calculate_fn):
        """
        This is mainly testing if the correct integration time is used while calculating
        the continuum sensitivity.
        """
        mock_calculate_fn.return_value = CalculatorResult(100, "uJy/beam")

        validated_input = {
            "num_stations": 100,
            "freq_centre_mhz": 150,
            "bandwidth_mhz": 118.518513664,
            "spectral_resolution_hz": 14467.592,
            "pointing_centre": "03:30:00 -30:00:00",
            "elevation_limit": 20,
            "integration_time_h": 1,
            "dm": 0.0,
            "pulse_period": 1.0,
            "intrinsic_pulse_width": 0.1,
            "pulsar_mode": "single_pulse",
        }

        expected_calculator_input = CalculatorInput(
            freq_centre_mhz=150,
            bandwidth_mhz=118.518513664,
            num_stations=100,
            pointing_centre="03:30:00 -30:00:00",
            integration_time_h=(69.12e-6 * u.s)
            .to(u.h)
            .value,  # PSS sampling time converted to hours
            elevation_limit=20,
        )

        convert_pss_input_and_calculate(validated_input)

        mock_calculate_fn.assert_called_once_with(expected_calculator_input)

    @mock.patch("ska_ost_senscalc.low.service.calculate_sensitivity")
    def test_num_stations_set_from_subarray_configuration(self, mock_calculate_fn):
        """
        num_stations should be derived from the subarray_configuration
        """
        mock_calculate_fn.return_value = CalculatorResult(100, "uJy/beam")

        user_input = {
            "subarray_configuration": "LOW_AA4_core_only",
            "freq_centre_mhz": 150,
            "bandwidth_mhz": 118.518513664,
            "spectral_resolution_hz": 14467.592,
            "pointing_centre": "03:30:00 -30:00:00",
            "elevation_limit": 20,
            "integration_time_h": 1,
            "dm": 0.0,
            "pulse_period": 1.0,
            "intrinsic_pulse_width": 0.1,
            "pulsar_mode": "folded_pulse",
        }

        expected_calculator_input = CalculatorInput(
            freq_centre_mhz=150,
            bandwidth_mhz=118.518513664,
            num_stations=224,  # Assert that this param is set
            pointing_centre="03:30:00 -30:00:00",
            integration_time_h=1,
            elevation_limit=20,
        )

        convert_pss_input_and_calculate(user_input)

        mock_calculate_fn.assert_called_once_with(expected_calculator_input)


# class TestWeightingForLine:
#     @mock.patch("ska_ost_senscalc.low.service.get_single_weighting_response")
#     def test_get_weighting_response_from_weighting_input(self, mock_weighting_fn):
#         user_input = {
#             "num_stations": 42,
#             "freq_centre_mhz": 400,
#             "total_bandwidth_khz": 3125,
#             "spectral_resolution_hz": 1808.4,
#             "pointing_centre": "13:25:28 -43:01:09",
#             "integration_time_h": 0.05,
#             "elevation_limit": 20,
#         }
#
#         mock_calculate_weighting_result = ContinuumWeightingResponse(
#             sbs_conv_factor=[1 * u.K / u.Jy],
#             weighting_factor=1,
#             beam_size=[],
#         )
#
#         mock_weighting_fn.return_value = mock_calculate_weighting_result
#
#         expected_weighting_response = ContinuumWeightingResponse(
#             sbs_conv_factor=[1 * u.K / u.Jy],
#             weighting_factor=1,
#             beam_size=[],
#         )
#
#         result = get_zoom_weighting_response(user_input)
#
#         assert result == expected_weighting_response
