import copy
from http import HTTPStatus
from unittest import mock

import pytest
from astropy import units as u
from astropy.coordinates import SkyCoord
from flask import json

from ska_ost_senscalc import get_openapi_spec
from ska_ost_senscalc.common.model import (
    ContinuumCalculatorAndWeightingInput,
    Weighting,
    WeightingSpectralMode,
)
from ska_ost_senscalc.subarray import LOWArrayConfiguration
from ska_ost_senscalc.utilities import Telescope

from ..conftest import DEFAULT_API_PATH

SUBARRAY_API = f"{DEFAULT_API_PATH}/low/subarrays"

calculate_required_params = {"num_stations": 512}


def test_subarrays_list(client):
    """Test the subarrays entry point."""
    response = client.get(SUBARRAY_API)
    response_body = json.loads(response.data)
    assert {
        "name": "LOW_AAstar_all",
        "label": "AA*",
        "n_stations": 307,
    } in response_body
    assert {
        "name": "LOW_AAstar_core_only",
        "label": "AA* (core only)",
        "n_stations": 199,
    } in response_body
    assert {
        "name": "LOW_AA4_core_only",
        "label": "AA4 (core only)",
        "n_stations": 224,
    } in response_body
    assert response.status_code == HTTPStatus.OK


class TestContinuum:
    url = f"{DEFAULT_API_PATH}/low/continuum/calculate"

    params = {
        "subarray_configuration": "LOW_AA4_all",
        "integration_time_h": 1,
        "pointing_centre": "10:00:00.0 -30:00:00.0",
        "elevation_limit": 20,
        "freq_centre_mhz": 200,
        "spectral_averaging_factor": 1,
        "bandwidth_mhz": 300,
        "weighting_mode": "uniform",
        "robustness": 0,
    }

    calculator_weighting_input = ContinuumCalculatorAndWeightingInput(
        freq_centre=u.Quantity(2.0e08, u.Hz),
        bandwidth_mhz=300.0,
        num_stations=512,
        pointing_centre=SkyCoord("10:00:00.0 -30:00:00.0", unit=(u.hourangle, u.deg)),
        integration_time_h=1.0,
        elevation_limit=20.0,
        telescope=Telescope.LOW,
        spectral_mode=WeightingSpectralMode.CONTINUUM,
        subarray_configuration=LOWArrayConfiguration.LOW_AA4_ALL,
        spectral_averaging_factor=1.0,
        n_subbands=1,
        weighting_mode=Weighting.UNIFORM,
        robustness=0.0,
        subband_freq_centres=None,
        taper=u.Quantity(0, u.arcsec),
    )

    # schema example response from swagger UI
    valid_response = {
        "continuum_sensitivity": {"value": 4.77839424123094, "unit": "uJy / beam"},
        "continuum_subband_sensitivities": [],
        "spectral_sensitivity": {"value": 666.5608566846619, "unit": "uJy / beam"},
        "warnings": [
            "The specified pointing contains at least one source brighter than 10.0 Jy. Your observation may be dynamic range limited."
        ],
        "spectropolarimetry_results": {
            "fwhm_of_the_rmsf": {"value": 0.09837671426218081, "unit": "rad / m2"},
            "max_faraday_depth_extent": {"value": 4.28191284692133, "unit": "rad / m2"},
            "max_faraday_depth": {"value": 222.0457075444506, "unit": "rad / m2"},
        },
        "continuum_weighting": {
            "weighting_factor": 10.331757457585987,
            "sbs_conv_factor": 3624408.8848622595,
            "confusion_noise": {"value": 6.328070885097382e-07, "limit_type": "value"},
            "beam_size": {
                "beam_maj_scaled": 0.0009294689852640001,
                "beam_min_scaled": 0.0006998047590761798,
                "beam_pa": 190.26451545511534,
            },
        },
        "spectral_weighting": {
            "weighting_factor": 12.524796556896934,
            "sbs_conv_factor": 1338319.6546982538,
            "confusion_noise": {"value": 2.560245257439271e-06, "limit_type": "value"},
            "beam_size": {
                "beam_maj_scaled": 0.0014414588620446558,
                "beam_min_scaled": 0.0012220440815741277,
                "beam_pa": 10.089813757113648,
            },
        },
        "weighted_result": {
            "weighted_continuum_sensitivity": {
                "value": 49.36921033712369,
                "unit": "uJy / beam",
            },
            "continuum_confusion_noise": {"value": 0.6328070885097382, "unit": "uJy"},
            "total_continuum_sensitivity": {
                "value": 49.37326578344224,
                "unit": "uJy / beam",
            },
            "continuum_synthesized_beam_size": {
                "value": "3.3' x 2.5'",
                "unit": "arcsec2",
            },
            "continuum_surface_brightness_sensitivity": {
                "value": 178948903.18017384,
                "unit": "uK",
            },
            "weighted_spectral_sensitivity": {
                "value": 8348.539122766324,
                "unit": "uJy / beam",
            },
            "spectral_confusion_noise": {"value": 2.560245257439271, "unit": "uJy"},
            "total_spectral_sensitivity": {
                "value": 8348.539515341332,
                "unit": "uJy / beam",
            },
            "spectral_synthesized_beam_size": {
                "value": "5.2' x 4.4'",
                "unit": "arcsec2",
            },
            "spectral_surface_brightness_sensitivity": {
                "value": 11173014521.40634,
                "unit": "uK",
            },
        },
    }

    @mock.patch("ska_ost_senscalc.low.api.convert_continuum_input_and_calculate")
    def test_happy_path(self, mock_service_fn, client):
        mock_service_fn.return_value = TestContinuum.valid_response

        response = client.get(TestContinuum.url, query_string=TestContinuum.params)
        response_body = json.loads(response.data)
        assert response.status_code == HTTPStatus.OK
        assert response_body == TestContinuum.valid_response

    @pytest.mark.parametrize(
        ["to_remove"],
        [("freq_centre_mhz",), ("weighting_mode",)],
    )
    @mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults_for_continuum")
    def test_response_is_validated(self, mock_service_fn, to_remove, client):
        param = copy.deepcopy(TestContinuum.params)
        del param[to_remove]
        mock_service_fn.return_value = param

        response = client.get(TestContinuum.url, query_string=TestContinuum.params)
        # TODO: looks totally wrong to me, can we refactor it to actually test something useful?
        # we expect an unexpected crash (500) which may happen for a million of reasons
        assert response.status_code == HTTPStatus.INTERNAL_SERVER_ERROR

    @mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults_for_continuum")
    @mock.patch("ska_ost_senscalc.low.api.convert_continuum_input_and_calculate")
    def test_calculate_with_params(self, mock_service_fn, mock_validate_fn1, client):
        mock_validate_fn1.return_value = self.calculator_weighting_input
        mock_service_fn.return_value = self.valid_response

        response = client.get(self.url, query_string=self.params)
        response_body = json.loads(response.data)

        assert response.status_code == HTTPStatus.OK
        assert response_body == self.valid_response

        mock_validate_fn1.assert_called_with(self.params)
        mock_service_fn.assert_called_with(self.calculator_weighting_input)

    @mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults_for_continuum")
    def test_calculate_handles_validation_error(self, mock_validate_fn, client):
        mock_validate_fn.side_effect = ValueError("test error")

        response = client.get(self.url, query_string=self.params)
        response_body = json.loads(response.data)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert response_body == {
            "detail": "test error",
            "title": "Validation Error",
        }

    @mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults_for_continuum")
    def test_multiple_validation_errors__are_reported_correctly(
        self, mock_validate_fn, client
    ):
        """
        Verify that multiple validation errors are reported as a
        semicolon-delimited string.
        """
        error_msgs = ["foo", "bar", "baz"]
        mock_validate_fn.side_effect = ValueError(*error_msgs)

        response = client.get(self.url, query_string=self.params)
        response_body = json.loads(response.data)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert response_body == {
            "detail": "foo;bar;baz",
            "title": "Validation Error",
        }

    @mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults_for_continuum")
    def test_calculate_handles_general_error(self, mock_service_fn, client):
        mock_service_fn.side_effect = RuntimeError()

        response = client.get(self.url, query_string=self.params)
        response_body = json.loads(response.data)

        assert response.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
        assert response_body == {"title": "Internal Server Error"}


class TestZoom:
    ZOOM_CALCULATE_API = f"{DEFAULT_API_PATH}/low/zoom/calculate"

    params = {
        "subarray_configuration": "LOW_AA4_all",
        "integration_time_h": 1.0,
        "pointing_centre": "10:00:00.0 -30:00:00.0",
        "elevation_limit": 20.0,
        "freq_centres_mhz": [200.0],
        "spectral_averaging_factor": 1.0,
        "spectral_resolutions_hz": [14.129],
        "total_bandwidths_khz": [24.414],
        "weighting_mode": "uniform",
        "robustness": 0.0,
    }

    valid_response = {
        "calculate": [
            {
                "freq_centre": {"value": 200, "unit": "MHz"},
                "spectral_sensitivity": {
                    "value": 13061.493233622128,
                    "unit": "uJy / beam",
                },
                "warning": None,
                "spectropolarimetry_results": {
                    "fwhm_of_the_rmsf": {
                        "value": 6318.611811653445,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth_extent": {
                        "value": 1.3983678812167422,
                        "unit": "rad / m2",
                    },
                    "max_faraday_depth": {
                        "value": 5454919.046074606,
                        "unit": "rad / m2",
                    },
                },
            }
        ],
        "weighting": [
            {
                "freq_centre": {"value": 200000000, "unit": "Hz"},
                "weighting_factor": 12.524796556896934,
                "sbs_conv_factor": 1338319.6546982538,
                "confusion_noise": {
                    "value": 0.000002560245257439271,
                    "limit_type": "value",
                },
                "beam_size": {
                    "beam_maj_scaled": 0.0014414588620446558,
                    "beam_min_scaled": 0.0012220440815741277,
                    "beam_pa": 10.089813757113648,
                },
            }
        ],
        "weighted_result": [
            {
                "weighted_spectral_sensitivity": {
                    "value": 0.16359254548040303,
                    "unit": "Jy / beam",
                },
                "spectral_confusion_noise": {
                    "value": 0.000002560245257439271,
                    "unit": "Jy",
                },
                "total_spectral_sensitivity": {
                    "value": 0.16359254550043711,
                    "unit": "Jy / beam",
                },
                "spectral_synthesized_beam_size": {
                    "value": "5.2 x 4.4",
                    "unit": "arcsec2",
                },
                "spectral_surface_brightness_sensitivity": {
                    "value": 218939.11900535336,
                    "unit": "K",
                },
            }
        ],
    }

    @mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults_for_zoom")
    @mock.patch("ska_ost_senscalc.low.api.convert_zoom_input_and_calculate")
    def test_calculate_with_params(self, mock_service_fn, mock_validate_fn, client):
        mock_validate_fn.return_value = self.params
        mock_service_fn.return_value = self.valid_response

        response = client.get(self.ZOOM_CALCULATE_API, query_string=self.params)

        response_body = json.loads(response.data)

        assert response.status_code == HTTPStatus.OK
        assert response_body == TestZoom.valid_response

        mock_validate_fn.assert_called_with(self.params)
        mock_service_fn.assert_called_with(self.params)

    @mock.patch("ska_ost_senscalc.low.api.validate_and_set_defaults_for_zoom")
    def test_calculate_handles_validation_error(self, mock_validate_fn, client):
        mock_validate_fn.side_effect = ValueError("test error")

        params = {
            "subarray_configuration": "LOW_AA4_all",
            "integration_time_h": 1.0,
            "pointing_centre": "10:00:00.0 -30:00:00.0",
            "elevation_limit": 20.0,
            "freq_centres_mhz": [200.0],
            "spectral_averaging_factor": 1.0,
            "spectral_resolutions_hz": [14.129],
            "total_bandwidths_khz": [24.414],
            "weighting_mode": "uniform",
            "robustness": 0.0,
        }

        response = client.get(self.ZOOM_CALCULATE_API, query_string=params)

        response_body = json.loads(response.data)

        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert response_body == {
            "detail": "test error",
            "title": "Validation Error",
        }

    @mock.patch("ska_ost_senscalc.low.api.convert_zoom_input_and_calculate")
    def test_calculate_handles_general_error(self, mock_service_fn, client):
        mock_service_fn.side_effect = RuntimeError()
        response = client.get(self.ZOOM_CALCULATE_API, query_string=self.params)

        response_body = json.loads(response.data)

        assert response.status_code == HTTPStatus.INTERNAL_SERVER_ERROR
        assert response_body == {"title": "Internal Server Error"}


class TestOpenAPISpec:
    @pytest.mark.parametrize(
        "openapi_enum,python_enum,fn",
        [
            (
                "LowSubarrayConfigurationForContinuum",
                LOWArrayConfiguration,
                str.upper,
            )
        ],
    )
    def test_openapi_enum_matches_python_enum(self, openapi_enum, python_enum, fn):
        """
        Verify that the LOW subarray OpenAPI enumeration matches the Python
        enumeration names, as any mismatch would be classed as a 400 client
        query validation error at runtime.
        """
        spec = get_openapi_spec("./static/openapi-low.yaml")

        python_enum_names = {e.name for e in python_enum}

        openapi_param = spec["components"]["parameters"][openapi_enum]
        openapi_enum_names = {fn(e) for e in openapi_param["schema"]["enum"]}
        # not all Python enums may be exposed/implemented yet, such as LOW
        # custom
        assert openapi_enum_names <= python_enum_names
