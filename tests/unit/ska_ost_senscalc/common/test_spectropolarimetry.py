from astropy.units import Quantity, isclose

from ska_ost_senscalc.common.spectropolarimetry import (
    _get_first_channel_central_frequency,
    _get_last_channel_central_frequency,
    calcuate_fwhm_of_the_rmsf,
    calculate_max_faraday_depth,
    calculate_max_faraday_depth_extent,
)


def test_calculate_fwhm_of_the_rmsf():
    assert isclose(
        calcuate_fwhm_of_the_rmsf(
            Quantity(10e6, "Hz"), Quantity(20e6, "Hz"), Quantity(10e5, "Hz")
        ),
        Quantity(1.543982e-2, "rad / m2"),
    )


def test_calculate_max_faraday_depth_extent():
    assert isclose(
        calculate_max_faraday_depth_extent(
            Quantity(10e6, "Hz"), Quantity(20e6, "Hz"), Quantity(10e5, "Hz")
        ),
        Quantity(2.09817e-2, "rad / m2"),
    )


def test_calculate_max_faraday_depth():
    assert isclose(
        calculate_max_faraday_depth(
            Quantity(10e6, "Hz"), Quantity(20e6, "Hz"), Quantity(10e5, "Hz")
        ),
        Quantity(3.580811e-2, "rad / m2"),
    )


def test_get_first_channel_central_frequency_hz():
    assert _get_first_channel_central_frequency(
        Quantity(80e6, "Hz"), Quantity(200e6, "Hz"), Quantity(10e6, "Hz")
    ) == Quantity(165e6, "Hz")


def test_get_last_channel_central_frequency_hz():
    assert _get_last_channel_central_frequency(
        Quantity(Quantity(80e6, "Hz"), "Hz"),
        Quantity(200e6, "Hz"),
        Quantity(10e6, "Hz"),
    ) == Quantity(235e6, "Hz")
