"""
Unit tests for the ska_ost_senscalc.mid_utilities module.
"""

import astropy.units as u
import pytest

from ska_ost_senscalc.common.beam import _get_surface_brightness_conversion_factor
from ska_ost_senscalc.common.model import BeamSize


def test_surface_brightness_conversion_factor_handles_multiple_frequency_units():
    """
    Test that the surface brightness conversion factor gives the same answer when
    frequency is given with a different unit.
    """
    beam_size = BeamSize(
        beam_min=0.002117630534494051 * u.deg,
        beam_maj=0.0016687410496727072 * u.deg,
        beam_pa=-9.335564851094302 * u.deg,
    )
    sbs_ghz = _get_surface_brightness_conversion_factor(1.4 * u.GHz, beam_size)
    sbs_hz = _get_surface_brightness_conversion_factor(1400000000 * u.Hz, beam_size)

    assert sbs_ghz.value == pytest.approx(1500, rel=100)
    assert sbs_ghz == sbs_hz
