import astropy.units as u
import pytest

from ska_ost_senscalc.common.model import WeightingSpectralMode
from ska_ost_senscalc.common.weighting_lookup import Weighting, WeightingTableFactory
from ska_ost_senscalc.subarray import LOWArrayConfiguration, MIDArrayConfiguration
from ska_ost_senscalc.utilities import Telescope

tableFactory = WeightingTableFactory()


def extract_reltonat(
    telescope,
    array_config,
    weighting_mode,
    spectral_mode,
    dec,
    taper=0.0 * u.arcsec,
    robustness=0,
):
    """
    function to extract the beam_weighting value calculated by RASCIL/casa in the lookup table and comparing it to
    out method. Only works if the declination value is present in the look up table - i.e. no interpolation is
    required. By including a frequency parameter we can pass the same params as we do to the Beam class.
    """
    filtered_table = tableFactory.get_table(
        telescope=telescope,
        array_config=array_config,
        spectral_mode=spectral_mode,
        weighting_mode=weighting_mode,
        taper=taper,
        robustness=robustness,
    )

    lookup_value = filtered_table._table["reltonat_casa"][
        (filtered_table._table["declination"] == dec.value)
    ][0]
    return lookup_value


beam_weighting_params_direct = [
    (
        {
            "telescope": Telescope.MID,
            "array_config": MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY,
            "weighting_mode": Weighting.ROBUST,
            "robustness": 0,
            "taper": 1024.0 * u.arcsec,
            "spectral_mode": WeightingSpectralMode.CONTINUUM,
        },
        -90 * u.deg,
    ),
    (
        {
            "telescope": Telescope.MID,
            "array_config": MIDArrayConfiguration.MID_AA4_SKA_ONLY,
            "weighting_mode": Weighting.UNIFORM,
            "taper": 64.0 * u.arcsec,
            "spectral_mode": WeightingSpectralMode.CONTINUUM,
        },
        -50 * u.deg,
    ),
    (
        {
            "telescope": Telescope.MID,
            "array_config": MIDArrayConfiguration.MID_AA4_ALL,
            "weighting_mode": Weighting.ROBUST,
            "robustness": 2,
            "taper": 4.0 * u.arcsec,
            "spectral_mode": WeightingSpectralMode.CONTINUUM,
        },
        10 * u.deg,
    ),
    (
        {
            "telescope": Telescope.LOW,
            "array_config": LOWArrayConfiguration.LOW_AA2_CORE_ONLY,
            "weighting_mode": Weighting.ROBUST,
            "robustness": 1,
            "spectral_mode": WeightingSpectralMode.CONTINUUM,
        },
        -90 * u.deg,
    ),
    (
        {
            "telescope": Telescope.LOW,
            "array_config": LOWArrayConfiguration.LOW_AA4_ALL,
            "weighting_mode": Weighting.UNIFORM,
            "spectral_mode": WeightingSpectralMode.CONTINUUM,
        },
        -50 * u.deg,
    ),
    (
        {
            "telescope": Telescope.LOW,
            "array_config": LOWArrayConfiguration.LOW_AA05_ALL,
            "weighting_mode": Weighting.ROBUST,
            "robustness": 2,
            "spectral_mode": WeightingSpectralMode.CONTINUUM,
        },
        10 * u.deg,
    ),
]


@pytest.mark.parametrize("params, dec", beam_weighting_params_direct)
def test_weighting_factor_direct(params, dec):
    """Validate beam weighting factor is correct using values present in the
    beam_weighting_sensitivity.csv lookup table (no interpolation required).
    """
    wTable = tableFactory.get_table(**params)
    result = wTable.get_weighting_factor(dec)
    assert result == pytest.approx(extract_reltonat(**params, dec=dec), rel=1e-6)


@pytest.mark.parametrize(
    "telescope,dec",
    [
        (Telescope.MID, 46 * u.deg),  # table max dec of 45, plus 1 degree
        (Telescope.MID, -90.00001 * u.deg),
        (Telescope.LOW, 50 * u.deg),  # table max dec of 45, plus 5 degrees
        (Telescope.LOW, -90.00001 * u.deg),
    ],
)
def test_weighting_factor_extrapolation_raises_runtime_error(telescope, dec):
    """Test weighting_factor raises a value error if declination
    value given is outside the minimum and maximum values present
    in the lookup table.
    """
    wTable = tableFactory.get_table(
        telescope=telescope,
        array_config=MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY,
        spectral_mode=WeightingSpectralMode.CONTINUUM,
        weighting_mode=Weighting.ROBUST,
        robustness=2,
        taper=64.0 * u.arcsec,
    )

    with pytest.raises(ValueError):
        _ = wTable.get_weighting_factor(dec)


@pytest.mark.parametrize(
    "params, frequency, dec, beam_maj_lookup, beam_min_lookup",
    [
        (
            {
                "telescope": Telescope.MID,
                "array_config": MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY,
                "weighting_mode": Weighting.ROBUST,
                "robustness": 1,
                "spectral_mode": WeightingSpectralMode.CONTINUUM,
            },
            350000000.0 * u.Hz,
            44 * u.deg,
            0.012840438620978736,
            0.0027618615434784246,
        ),
        (
            {
                "telescope": Telescope.MID,
                "array_config": MIDArrayConfiguration.MID_AA4_SKA_ONLY,
                "weighting_mode": Weighting.UNIFORM,
                "taper": 64.0 * u.arcsec,
                "spectral_mode": WeightingSpectralMode.LINE,
            },
            500000000 * u.Hz,
            -70 * u.deg,
            0.020612344230645393,
            0.01816289360666419,
        ),
    ],
)
def test_beam_size_method_returns_expected_values_direct(
    params, frequency, dec, beam_maj_lookup, beam_min_lookup
):
    """test that beam_size method returns a BeamSize object with expected values
    with no interpolation required. Note, this test will fail if the lookup table changes
    """
    wTable = tableFactory.get_table(**params)
    beam_size = wTable.get_beam_size(frequency, dec)

    expected_beam_maj = beam_maj_lookup * u.deg * 1.4 * u.GHz / frequency.to(u.GHz)

    expected_beam_min = beam_min_lookup * u.deg * 1.4 * u.GHz / frequency.to(u.GHz)

    assert round(expected_beam_maj.value, 15) == round(beam_size.beam_maj.value, 15)
    assert round(expected_beam_min.value, 15) == round(beam_size.beam_min.value, 15)
    assert expected_beam_maj.unit == beam_size.beam_maj.unit


def test_beam_size_method_returns_expected_values_interpolation():
    freq = 350000000.0 * u.Hz
    wTable = tableFactory.get_table(
        telescope=Telescope.MID,
        array_config=MIDArrayConfiguration.MID_AA4_MEERKAT_ONLY,
        spectral_mode=WeightingSpectralMode.CONTINUUM,
        weighting_mode=Weighting.ROBUST,
        robustness=1,
    )
    beam_size = wTable.get_beam_size(freq, 20 * u.deg)

    # These two values are taken from the lookup table row which matches the
    # parameters above with declinations of 30 and 10 (ie +/- 10 from the param
    # dec)
    cleanbeam_maj_from_lookup_dec_10 = 0.0047115287990772605
    cleanbeam_maj_from_lookup_dec_30 = 0.007100861916890273

    result_derived_value = beam_size.beam_maj * freq.to(u.GHz) / 1.4 * u.GHz

    assert (
        cleanbeam_maj_from_lookup_dec_30
        > result_derived_value.value
        > cleanbeam_maj_from_lookup_dec_10
    )


@pytest.mark.parametrize(
    "telescope,subarray,freq,dec",
    [
        (Telescope.MID, MIDArrayConfiguration.MID_AA4_ALL, 650e6 * u.Hz, 44.9 * u.deg),
        (Telescope.LOW, LOWArrayConfiguration.LOW_AA4_ALL, 100e6 * u.Hz, 48.9 * u.deg),
    ],
)
def test_weighting_factor_extrapolation(telescope, subarray, freq, dec):
    """Test weighting_factor extrapolates beyond table limits."""
    wTable = tableFactory.get_table(
        telescope=telescope,
        array_config=subarray,
        spectral_mode=WeightingSpectralMode.CONTINUUM,
        weighting_mode=Weighting.ROBUST,
        robustness=0,
    )

    # no need to check extrapolated values, just confirm that it doesn't raise
    # a ValueError
    _ = wTable.get_weighting_factor(dec)
    _ = wTable.get_beam_size(freq, dec)

    # whereas equalling or exceeding the bounds does
    with pytest.raises(ValueError):
        _ = wTable.get_weighting_factor(dec + 1.0)
    with pytest.raises(ValueError):
        _ = wTable.get_beam_size(freq, dec + 1.0)
