"""
Unit tests for the ska_ost_senscalc.subarray module.
"""

import json
from pathlib import Path

import pytest

from ska_ost_senscalc.subarray import MidSubarray, MidSubarraySchema, SubarrayStorage
from ska_ost_senscalc.utilities import MEERKAT_NDISHES_MAX, SKA_NDISHES_MAX, Telescope

BASE_DIR = Path(__file__).parents[3].resolve()
MID_CONFIG_FILE_PATH = Path(
    BASE_DIR, "src", "ska_ost_senscalc", "static", "subarrays", "mid"
)

# tell black to not format the following dicts
# fmt: off
mid_full_subarray = {
    "ids": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38,
            39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
            58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76,
            77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95,
            96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
            112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126,
            127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141,
            142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156,
            157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171,
            172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186,
            187, 188, 189, 190, 191, 192, 193, 194, 195, 196],
    "name": "mid_full_subarray",
    "label": "Mid Test AA4",
    "configuration": str(MID_CONFIG_FILE_PATH / "ska1mid.cfg"),
    "md5_checksum": "d41d8cd98f00b204e9800998ecf8427e"
}

mid_meerkat_subarray = {
    "ids": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
            20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
            37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
            54, 55, 56, 57, 58, 59, 60, 61, 62, 63],
    "name": "mid_meerkat_subarray",
    "label": "Mid Test MeerKAT AA4",
    "configuration": str(MID_CONFIG_FILE_PATH / "ska1mid.cfg"),
    "md5_checksum": "d41d8cd98f00b204e9800998ecf8427e"
}

mid_ska_subarray = {
    "ids": [64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80,
            81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,
            100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114,
            115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129,
            130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144,
            145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159,
            160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174,
            175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189,
            190, 191, 192, 193, 194, 195, 196],
    "name": "mid_ska_subarray",
    "label": "Mid Test SKA AA4",
    "configuration": str(MID_CONFIG_FILE_PATH / "ska1mid.cfg"),
    "md5_checksum": "d41d8cd98f00b204e9800998ecf8427e"
}
# fmt: on

mid_subarrays = [mid_full_subarray, mid_meerkat_subarray, mid_ska_subarray]


@pytest.fixture
def subarray_storage_path(tmp_path):
    """Temporary test path to subarrays folder"""
    sub_path = tmp_path / "subarrays"
    sub_path.mkdir()
    return sub_path


@pytest.fixture
def mid_subarray_storage_path(subarray_storage_path):
    """Temporary test path to subarrays/mid folder where
    Mid subarray JSON files are stored"""
    mid_path = subarray_storage_path / "mid"
    mid_path.mkdir()
    return mid_path


@pytest.fixture
def mid_subarray_storage(mid_subarray_storage_path):
    return SubarrayStorage(Telescope.MID, mid_subarray_storage_path)


@pytest.fixture(autouse=True)
def create_test_subarray_files(mid_subarray_storage_path):
    """Populate a temporary test directory with subarray JSON files"""
    for sub in mid_subarrays:
        sub_file = mid_subarray_storage_path / (sub["name"] + ".json")
        sub_file.touch()
        sub_file.write_text(json.dumps(sub))


def test_local_storage_listing(mid_subarray_storage):
    """Verify storage lists subarray files properly"""
    subarray_list = mid_subarray_storage.list()
    for sub in mid_subarrays:
        listed_sub = next(x for x in subarray_list if x.name == sub["name"])
        assert listed_sub.name == sub["name"]
        assert listed_sub.label == sub["label"]
        assert listed_sub.ids == sub["ids"]
        assert listed_sub.configuration == sub["configuration"]


@pytest.mark.parametrize("subarray_json", mid_subarrays)
def test_local_storage_load_data(subarray_json, mid_subarray_storage):
    """Verify that the storage instance load subarray files and transforms them to Subarray objects properly"""
    subarray_obj = MidSubarray(**subarray_json)
    assert mid_subarray_storage.load_by_label(subarray_obj.label) == subarray_obj


def test_local_storage_load_non_existing_data(mid_subarray_storage):
    """Verify that an exception is raised when the storage file is not found"""
    with pytest.raises(ValueError):
        mid_subarray_storage.load_by_label("aaa")


@pytest.mark.parametrize(
    "subarray_obj, expected_nmeer, expected_nska",
    [
        (
            MidSubarray(**mid_full_subarray),
            MEERKAT_NDISHES_MAX,
            SKA_NDISHES_MAX,
        ),
        (MidSubarray(**mid_ska_subarray), 0, SKA_NDISHES_MAX),
        (MidSubarray(**mid_meerkat_subarray), MEERKAT_NDISHES_MAX, 0),
    ],
)
def test_subarray_class_properties(subarray_obj, expected_nmeer, expected_nska):
    """Check that the two convenience properties return the correct number of antennas"""
    assert subarray_obj.n_ska == expected_nska
    assert subarray_obj.n_meer == expected_nmeer


def test_subarrayschema_loads_subarrays():
    """Test that the marshmallow schema works: deserialization"""
    subarray_schema = MidSubarraySchema()
    assert MidSubarray(**mid_full_subarray) == subarray_schema.load(mid_full_subarray)
    assert MidSubarray(**mid_ska_subarray) == subarray_schema.load(mid_ska_subarray)
    assert MidSubarray(**mid_meerkat_subarray) == subarray_schema.load(
        mid_meerkat_subarray
    )


def test_subarrayschema_error_on_bad_checksum():
    """Test that the SubarraySchema throws an error on invalid checksum"""
    bad_checksum = "bad??checksum"
    subarray_bad_checksum_data = {
        "ids": [0, 1],
        "name": "bad_checksum_subarray",
        "label": "Bad Checksum Subarray",
        "configuration": str(MID_CONFIG_FILE_PATH / "ska1mid.cfg"),
        "md5_checksum": bad_checksum,
    }
    subarray_schema = MidSubarraySchema()
    with pytest.raises(ValueError) as err:
        _ = subarray_schema.load(subarray_bad_checksum_data)
    assert bad_checksum in str(err)


@pytest.mark.parametrize("subarray_json", mid_subarrays)
def test_subarrayschema_dumps_subarrays(subarray_json):
    """Test that the marshmallow schema works: serialization"""
    subarray_schema = MidSubarraySchema()
    obj_to_dump = MidSubarray(**subarray_json)
    assert subarray_json == subarray_schema.dump(obj_to_dump)


def test_filename_label_mapping(mid_subarray_storage):
    """Test the filename_label_mapping method returns the files in the folder and correct labels"""
    mapping = mid_subarray_storage.filename_label_mapping()
    assert list(mapping.keys()) == sorted([sub["name"] for sub in mid_subarrays])
    for sub in mid_subarrays:
        assert mapping[sub["name"]] == sub["label"]
