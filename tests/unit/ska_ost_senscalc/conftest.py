from importlib.metadata import version

import pytest

from ska_ost_senscalc import create_app

SENSCALC_MAJOR_VERSION = version("ska-ost-senscalc").split(".")[0]
# Default as it uses the default namespace. When deployed to a different namespace the first part will change to that namespace.
DEFAULT_API_PATH = f"ska-ost-senscalc/senscalc/api/v{SENSCALC_MAJOR_VERSION}"


@pytest.fixture
def client():
    """As explained in the Flask documentation: https://flask.palletsprojects.com/en/1.1.x/testing/"""
    connexion = create_app()
    connexion.app.config["TESTING"] = True
    with connexion.app.test_client() as client:
        yield client
