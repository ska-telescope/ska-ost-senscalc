Changelog
==========

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

Unreleased
**********
* [BREAKING] Changed `/mid/continuum/calculate` endpoint to combine weighting and calculate params and return sensitivities, continuum and spectral weighting results, and weighted result (for ui display) where business logic is moved from `ska-ost-senscalc-ui` 
* [BREAKING] Removed Mid Continuum Weighting endpoint
* [BREAKING] Changed `/low/zoom/calculate` endpoint to combine weighting and calculate params and return sensitivities, spectral weighting results, and weighted result (for ui display) where business logic is moved from `ska-ost-senscalc-ui`
* [BREAKING] Removed Low Zoom Weighting endpoint
* [BREAKING] Changed `/low/continuum/calculate` endpoint to combine weighting and calculate params and return sensitivities, continuum and spectral weighting results, and weighted result (for ui display) where business logic is moved from `ska-ost-senscalc-ui` 
* [BREAKING] Removed Low Continuum Weighting endpoint
* [BREAKING] Changed `/mid/zoom/calculate` endpoint to combine weighting and calculate params and return original calculate, original weghting, and weighted result (for ui display) where business logic is moved from `ska-ost-senscalc-ui`
* [BREAKING] Removed MID Zoom Weighting endpoint
* Added subbands calculated results in calculated results section of response for Low Continuum
* Changed low continuum calculate warning to warnings and to be an array of string instead of a string
* Added Generation of subband_freq_centres_mhz if not provided but n_subbands > 1 
* Changed location of classes in low and common services into model.py
* Changed some of the validation logic to handle subband_freq_centres_mhz and n_subbands inputs


10.2.0
**********

* Removed dependency on PyEphem
* Update the lookup tables to add the following new subarray configurations to the MID calculator: 
  * Mid_inner_r125m_AA*
  * Mid_inner_r500m_AA*
  * Mid_inner_r2km_AA*
  * Mid_inner_r20km_AA*
  * Mid_inner_r125m_AA4
  * Mid_inner_r500m_AA4
  * Mid_inner_r2km_AA4
  * Mid_inner_r20km_AA4
  * MID Calculator is refactored to remove Calculator class and instead use a number of functions

10.1.0

*****

* Low sensitivity calculator speed improvement: Update SEFDTable class to be HDF5-backed instead of sqlite-backed
  * Smaller database: 894.7 MB -> 36.8 MB
  * Faster runtime
  * Bug fix for multiple LST steps

10.0.0

*****

Major breaking changes to the calculate and weighting APIs to make them consistent and easier to use.

* [BREAKING] `/mid/calculate` endpoint is now split into `/mid/zoom/calculate` and `/mid/continuum/calculate`
  * Response body of `/mid/continuum/calculate` returns a single object with the continuum and spectral sensitivities/integration times, plus subband results if applicable
  * Response body of `/mid/zoom/calculate` returns an array of continuum and spectral sensitivities/integration times for each zoom window
* [BREAKING] `/low/zoom/calculate` now also returns an array of results (and likewise accepts an array of `freq_centres_mhz`)
* [BREAKING] The calculator mode (continuum, zoom, pss) and the spectral mode (continuum or line/spectral) are now distinguished in the API.
* [BREAKING] `/mid/weighting` endpoint is now split into `/mid/continuum/weighting` and `/mid/zoom/weighting`. Similar to calculate, an array is returned for zoom.
* [BREAKING] `/low/{spectral_mode}/weighting` has been split into explicitly separate `/low/zoom/weighting` and `/low/continuum/weighting` with separate params (eg only subbands for continuum)
* [BREAKING] Several parameters renamed to make consistent
  * Parameters have units appended, eg`sensitivity` -> `sensitivity_jy`
  * Parameters that are arrays are plural, eg `subband_sensitivities_jy`
  * Variations of frequency changed to `freq_centre_<unit>`
  * Low `duration` changed to `integration_time_h`
  * `el` changed to `elevation_limit`
* [BREAKING] Response bodies updated to use same field names, at the top level of the object (ie no more `data` and `status` fields for mid).
* [BREAKING] Response quantities now are an object with value and unit, rather than a number with an implicit unit.
 