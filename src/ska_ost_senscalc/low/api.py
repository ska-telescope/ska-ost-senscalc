"""
These functions map to the API paths, with the returned value being the API response

Connexion maps the function name to the operationId in the OpenAPI document path
"""

import logging
from http import HTTPStatus

from ska_ost_senscalc.common.api import ResponseTuple, error_handler
from ska_ost_senscalc.common.model import ContinuumSensitivityCalculateResponse
from ska_ost_senscalc.low.service import (
    PSSSensitivityResponse,
    ZoomSensitivityResponse,
    convert_continuum_input_and_calculate,
    convert_pss_input_and_calculate,
    convert_zoom_input_and_calculate,
    get_subarray_response,
)
from ska_ost_senscalc.low.validation import (
    validate_and_set_defaults_for_continuum,
    validate_and_set_defaults_for_pss,
    validate_and_set_defaults_for_zoom,
)

LOGGER = logging.getLogger("senscalc")


@error_handler
def continuum_calculate(
    **kwargs,
) -> ResponseTuple[ContinuumSensitivityCalculateResponse]:
    """
     HTTP GET request handler for /api/low/continuum/calculate endpoint.

    Combines the request parameters for xcalculator and weighting calculations and returns a dictionary
    with the sensitivities, the weighting continuum and spectral results, and the transformed results which
    combine sensitivity with the weighting results for the respective spectral mode into a format easy to use
    for the front-end.

    :param kwargs: the HTTP parameters
    :return: a tuple of the response body (which is either a ContinuumCalculateResponse or an ErrorResponse)
        and HTTP status, which Connexion will wrap into a Response
    """
    validated_and_updated_params = validate_and_set_defaults_for_continuum(kwargs)
    response = convert_continuum_input_and_calculate(validated_and_updated_params)
    return (
        response,
        HTTPStatus.OK,
    )


@error_handler
def subarrays():
    """
    Function that GET requests to the /api/low/subarrays are routed to.

    Returns a response containing a list of available subarrays
    """
    return (
        get_subarray_response(),
        HTTPStatus.OK,
    )


@error_handler
def zoom_calculate(
    **kwargs,
) -> ResponseTuple[ZoomSensitivityResponse]:
    """
    HTTP GET request handler for /api/low/zoom/calculate endpoint. Combines the request parameters from
    /api/low/zoom/calculate and /api/low/zoom/weighting and creates a dictionary with
    the responses from the original combined requests, so it can be used to calculate the transformed results for low
    zoom. It then combines and transforms the responses into the results returned to the UI, which is returned along the weighting
    response and the warnings from the original calculate response.

    :param kwargs: the HTTP parameters
    :return: a tuple of the response body (which is either a :class:`ska_ost_senscalc.low.service.ZoomSensitivityResponse`
        or an :class:`ErrorResponse`) and HTTP status, which Connexion will wrap into a Response
    """

    validated = validate_and_set_defaults_for_zoom(kwargs)

    response = convert_zoom_input_and_calculate(validated)

    return response, HTTPStatus.OK


@error_handler
def pss_calculate(**kwargs) -> ResponseTuple[PSSSensitivityResponse]:
    """
    Function which HTTP GET requests to /api/low/pss/calculate are routed to.

    :param kwargs: the HTTP parameters
    :return: a tuple of the response body (which is either a :class:`ska_ost_senscalc.low.service.SensitivityResponse`
        or an :class:`ErrorResponse`) and HTTP status, which Connexion will wrap into a Response
    """
    validated_params = validate_and_set_defaults_for_pss(kwargs)

    return (
        convert_pss_input_and_calculate(validated_params),
        HTTPStatus.OK,
    )
