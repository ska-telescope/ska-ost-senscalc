#
# CAR_OCI_REGISTRY_HOST, DOCKER_REGISTRY_USER and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST (=artefact.skao.int) and overwrites
# DOCKER_REGISTRY_USER and PROJECT to give a final Docker tag of
# artefact.skao.int/ska-ost-senscalc
#
CAR_OCI_REGISTRY_HOST = artefact.skao.int
DOCKER_REGISTRY_USER:=ska-telescope
PROJECT = ska-ost-senscalc

# Set sphinx documentation build to fail on warnings (as it is configured
# in .readthedocs.yaml as well)
DOCS_SPHINXOPTS ?= -W --keep-going

# set following to 'info'|'debug'|'warning']'error'|'critical'
LOG_LEVEL = error

# include makefile to pick up the standard Make targets from the submodule
-include .make/base.mk
-include .make/python.mk
-include .make/oci.mk
-include .make/k8s.mk
-include .make/helm.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

#
# IMAGE_TO_TEST defines the tag of the Docker image to test
#
IMAGE_TO_TEST = $(CAR_OCI_REGISTRY_HOST)/$(strip $(OCI_IMAGE)):$(VERSION)

# Set the k8s test command run inside the testing pod to only run the component
# tests (no k8s pod deployment required for unit tests)
K8S_TEST_TEST_COMMAND = KUBE_NAMESPACE=$(KUBE_NAMESPACE) pytest ./tests/component -vv | tee pytest.stdout

# Set python-test make target to run unit tests and not the component tests
PYTHON_TEST_FILE = tests/unit/
PYTHON_VARS_AFTER_PYTEST = -vv

K8S_CHART = ska-ost-senscalc-umbrella

# unset defaults so settings in pyproject.toml take effect
PYTHON_SWITCHES_FOR_BLACK =
PYTHON_SWITCHES_FOR_ISORT =
PYTHON_LINE_LENGTH = 88

# Disable warning, convention, and refactoring messages
# Disable errors:
PYTHON_SWITCHES_FOR_FLAKE8=--ignore=A003,FS001,FS002,FS003,T101,W503,W391,E266,E402,E501,E731,F541,F841,RST304
PYTHON_SWITCHES_FOR_PYLINT=--disable=C,R,W,E

# Fail the docs build if there is a warning (eg if the autoimports are not configured)
# https://www.sphinx-doc.org/en/master/man/sphinx-build.html#cmdoption-sphinx-build-W
DOCS_SPHINXOPTS=-W --keep-going

pull:  ## download the application image
	docker pull $(IMAGE_TO_TEST)

up:
	poetry run gunicorn --chdir src -w 4 \
		--threads 4 --bind 0.0.0.0:5000 --logger-class=run.UniformLogger --log-level=$(LOG_LEVEL) run:app
# without gunicorn
#	docker run --rm -d -p 5000:5000 --name=$(CONTAINER_NAME) $(IMAGE_TO_TEST) poetry run flask run -h 0.0.0.0

down:  ## stop develop/test environment and any interactive session
	docker stop $(PROJECT) || true


# For the test, dev and integration environment, use the freshly built image in the GitLab registry
ENV_CHECK := $(shell echo $(CI_ENVIRONMENT_SLUG) | egrep 'test|dev|integration')
ifneq ($(ENV_CHECK),)
K8S_CHART_PARAMS = --set ska-ost-senscalc.rest.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set ska-ost-senscalc.rest.image.registry=$(CI_REGISTRY)/ska-telescope/ost/ska-ost-senscalc
endif

# For the staging environment, make k8s-install-chart-car will pull the chart from CAR so we do not need to
# change any values

# In staging environment get oci image from CAR
ENV_CHECK := $(shell echo $(CI_ENVIRONMENT_SLUG) | egrep staging)
ifneq ($(ENV_CHECK),)
K8S_CHART_PARAMS =
endif

post-set-release:
	sed -i"" -e "s/^\([[:blank:]]*\)version: \".*\"/\1version: \"$(VERSION)\"/" src/ska_ost_senscalc/static/openapi-mid*.yaml;
	sed -i"" -e "s/^\([[:blank:]]*\)version: \".*\"/\1version: \"$(VERSION)\"/" src/ska_ost_senscalc/static/openapi-low*.yaml

# The docs build fails unless the ska-ost-senscalc package is installed locally as importlib.metadata.version requires it.
docs-pre-build:
	poetry install --only-root

diagrams:  ## recreate PlantUML diagrams whose source has been modified
	mkdir -p docs/src/diagrams
	@for i in $$(git diff --name-only -- '*.puml'); \
	do \
		echo "Recreating $${i%%.*}.svg"; \
		cat $$i | docker run --rm -i think/plantuml -tsvg - > $${i%%.*}.svg; \
	done
	docker run -v $(CURDIR):/data rlespinasse/drawio-export --format=svg --on-changes --remove-page-suffix docs/src/diagrams
