.. _design:

======
Design
======

Goals and Considerations
------------------------
A prime consideration in the design of the calculator is 
exactly how the user will interface with it. Some users may prefer a 
simple, accessible interface, e.g. a web page, while others may prefer 
to be able to download a GUI to their own device. Some may even prefer 
to access the calculator API directly to use with their own code. 
Importantly, however, the sensitivity calculator will ultimately be 
part of some larger observing tool. The calculator is expected to provide the 
user with a report for attachment to their observing proposal 
supporting their use of the telescope. After speaking to the 
developer of the ALMA Observing Support Tool, it became clear that 
the vast majority of their users would use the web-based tool where 
possible and nearly always have internet access when wanting to use 
the tool. Therefore it was decided that the prototype calculator should 
use a web-based interface to demonstrate functionality, but feature a 
distinct front- and back-end, allowing the interface to be modified, 
or for other interfaces to be added (if needed) as the project evolves. 
Because the calculator is publicly available via the SKA GitLab, anyone 
who wants to directly interact with the source code can do so.

The scientific model behind the sensitivity calculation will be  
updated as the life-cycle of the telescope progresses. At the time of 
writing it is based on the calculation framework of 'SKA1: Design Baseline Description - SKA-TEL-SKO-0001075'
and 'SKA1 System Performance Assessment Report - SKA-TEL-SKO-0001089',
with some additional information from the earlier document
`Anticipated SKA1 Science Performance 
<https://astronomers.skatelescope.org/wp-content/uploads/2017/10/SKA-TEL-SKO-0000818-01_SKA1_Science_Perform.pdf>`_.
Once the telescope is live, we should be able to actively record, for 
example, the system temperature. This, among other possible developments, 
will change how the sensitivity calculator functions. In addition, with 
the current calculator being a prototype, a number of features will 
certainly be added as time goes on. With all of this considered, it 
is sensible to maintain a modular design for the calculator back-end, 
where functionality is separated into independent modules. This means 
that if, say, the model describing the receiver temperature for SKA1 
dishes is changed, the relevant code can easily be modified and the 
rest of the program should still run without any issues.

One of the desired features of the calculator is to be able to both 
calculate sensitivity, given an on-source integration time (and the 
other required parameters) and calculate the integration time required 
to reach a given sensitivity. The user should also be offered a range 
of different observing modes, so they can calculate e.g. total continuum 
noise, line noise, etc. These observing modes are not mutually exclusive, 
however. A user may be interested in performing a continuum observation 
with a number of zooms and would therefore want to know the sensitivity 
they could obtain in each case. Allowing for this while also allowing 
the user to swap between calculating sensitivity and integration time 
potentially makes both the front- and back-end design quite complicated.
The solution ultimately was to separate out the different observing modes 
into individual tabs.
