.. _common-model:

*****************************
ska_ost_senscalc.common.model
*****************************

.. automodule:: ska_ost_senscalc.common.model
    :members:
