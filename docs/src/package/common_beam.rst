.. _common-beam:

****************************
ska_ost_senscalc.common.beam
****************************

.. automodule:: ska_ost_senscalc.common.beam
    :members: calculate_weighting
