.. _weighting:

========================
Weighting Implementation
========================

Similar to the LOW Sensitivity Calculator, the weighting implementation is functional in nature, relying on pure
functions which do not mutate state. The diagram below describes the different aspects that are brought together in
the weighting result.


.. figure:: diagrams/export/beam_function_view.svg
   :align: center

Function flow for calculating weighting parameters for Mid and Low telescopes


The Python modules can also been seen below. Mostly the implementation uses module level pure functions, and makes use of
classes only for data objects and to encapsulate the look up tables.


.. figure:: diagrams/export/beam_module_diagram.svg
   :align: center
