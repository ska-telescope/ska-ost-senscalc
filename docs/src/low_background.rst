.. _low_background:

===========================
LOW Theoretical Background
===========================

The theoretical sensitivity is computed using direction, frequency, and LST-dependent SEFD values following the
procedure described in `Sokolowski et al. (2022) <https://doi.org/10.1017/pasa.2021.63>`_.
See the linked paper for more details.

Sensitivity of a beamformed observation
=======================================

The LOW sensitivity calculator backend uses the modified radiometer equation to estimate
the rms noise achievable in a coherently beamformed tied-array observation. In the
beamformed observing mode, the Low sensitivity calculator supports two types of
sensitivity estimates: folded-pulse sensitivity and single-pulse sensitivity.

Folded-pulse sensitivity
------------------------

Following
`Morello et al. (2020) <https://ui.adsabs.harvard.edu/link_gateway/2020MNRAS.497.4654M/doi:10.1093/mnras/staa2291>`_,
the folded-pulse sensitivity of a coherently beamformed tied-array observation with
:math:`\mathrm{N}_\mathrm{ant}` is given by the equation

.. math::
    \mathrm{S}_\mathrm{fps} = \frac{\beta}{\epsilon_{fps}} \frac{\mathrm{SEFD}_\mathrm{I}}{\sqrt{n_p \mathrm{t}_\mathrm{int} \Delta\nu \mathrm{N}_\mathrm{ant}^2 }} \sqrt{\frac{W}{P-W}}.
    :label: fps_radiometer_eq

In the above equation,

* :math:`\beta` is the digitisation loss. Following `Kouwenhoven & Voute (2001) <https://ui.adsabs.harvard.edu/link_gateway/2001A&A...378..700K/doi:10.1051/0004-6361:20011226>`_, we take :math:`\beta=1.05` for 8-bit quantisation.
* :math:`\epsilon_{fps}` is the search efficiency. Following `Morello et al. (2020) <https://ui.adsabs.harvard.edu/link_gateway/2020MNRAS.497.4654M/doi:10.1093/mnras/staa2291>`_, for FFT-based searches, we take :math:`\epsilon_{fps}=0.70`.
* :math:`\mathrm{SEFD}_\mathrm{I}` is the System Equivalent Flux Density for Stokes I in Jy.
* :math:`n_p` is the number of polarizations summed.
* :math:`\mathrm{t}_\mathrm{int}` is the observation duration in seconds.
* :math:`\Delta \nu` is the observation bandwidth in Hz.
* :math:`\mathrm{N}_\mathrm{ant}` is the number of antennas.
* :math:`\mathrm{W}` is the observed pulse width in milliseconds.
* :math:`\mathrm{P}` is the pulse period in milliseconds.

A pulse observed at the telescope is broader than the intrinsic pulse width due to propagation effects in the interstellar medium (ISM) like dispersion and scattering.

Pulse broadening (in milliseconds) due to dispersion measure :math:`\mathrm{DM}` (in :math:`\mathrm{pc}~\mathrm{cm}^{-3}`) can be calculated using the relation (`Lorimer & Kramer 2004 <https://ui.adsabs.harvard.edu/abs/2004hpa..book.....L/abstract>`_)

.. math::
    \mathrm{W}_\mathrm{DM} = \mathrm{K}_\mathrm{DM} \times \mathrm{DM} \times \frac{\Delta \nu}{\nu^3},

where :math:`\mathrm{K}_\mathrm{DM}=8.3 \times 10^6~\mathrm{ms}`, :math:`\Delta \nu` is the channel frequency resolution in MHz, and :math:`\nu` is the representative frequency (usually taken to be the band centre) in MHz.

Pulse broadening due to scattering in the ISM (:math:`\mathrm{W}_\mathrm{scat}`) is given by the relation (`Bhat et al. 2004 <https://ui.adsabs.harvard.edu/link_gateway/2004ApJ...605..759B/doi:10.1086/382680>`_)

.. math::
    \log (\mathrm{W}_\mathrm{scat}) = -6.46 + 0.154 \log (\mathrm{DM}) + 1.07 \log (\mathrm{DM})^2 - 3.86 \log (\nu_\mathrm{GHz})

where :math:`\nu_\mathrm{GHz}` is frequency in GHz.

The observed pulse width :math:`\mathrm{W}` is related to the intrinsic pulse width :math:`\mathrm{W}_\mathrm{int}` by the relation

.. math::
    \mathrm{W} = \sqrt{\mathrm{W}_\mathrm{int}^2 + \mathrm{W}_\mathrm{DM}^2 + \mathrm{W}_\mathrm{scat}^2}.

Note that the equation for folded-pulse sensitivity :eq:`fps_radiometer_eq` can be expressed in terms of Stokes I continuum sensitivity.
If :math:`\mathrm{S}_\mathrm{cont}` is the continuum sensitivity, assuming that the observation is carried out using the same number of stations,
bandwidth, and observation duration, the folded-pulse sensitivity can then be rewritten as

.. math::
    \mathrm{S}_\mathrm{fps} = \frac{\beta}{\epsilon_{fps}} \mathrm{S}_\mathrm{cont} \sqrt{\frac{\mathrm{N}_\mathrm{ant}-1}{\mathrm{N}_\mathrm{ant}}} \sqrt{\frac{\mathrm{W}}{\mathrm{P}-\mathrm{W}}}.

Single-pulse sensitivity
------------------------
Following `McGaughlin & Cordes (2003) <https://arxiv.org/abs/astro-ph/0304365>`_, single-pulse
sensitivity of a beamformed observation is given by the relation

.. math::
    \mathrm{S}_\mathrm{sps} = \frac{\beta}{\epsilon_{sps}} \frac{\mathrm{SEFD}_\mathrm{I}}{\sqrt{n_p \Delta\nu \mathrm{N}_\mathrm{ant}^2 W}} .
    :label: sps_radiometer_eq

In the above equation,

* :math:`\beta` is the digitisation loss. Following `Kouwenhoven & Voute (2001) <https://ui.adsabs.harvard.edu/link_gateway/2001A&A...378..700K/doi:10.1051/0004-6361:20011226>`_, we take :math:`\beta=1.05` for 8-bit quantisation.
* :math:`\epsilon_{sps}` is the search efficiency. Following `Morello et al. (2020) <https://ui.adsabs.harvard.edu/link_gateway/2020MNRAS.497.4654M/doi:10.1093/mnras/staa2291>`_, we take :math:`\epsilon_{sps}=0.93`.
* :math:`\mathrm{SEFD}_\mathrm{I}` is the System Equivalent Flux Density for Stokes I in Jy.
* :math:`\Delta \nu` is the observation bandwidth in Hz.
* :math:`\mathrm{N}_\mathrm{ant}` is the number of antennas.
* :math:`\mathrm{W}` is the observed pulse width in seconds (see previous section on the impact of scattering and DM on the pulse width).

Note that the equation for single-pulse sensitivity :eq:`sps_radiometer_eq` can be
expressed in terms of Stokes I continuum sensitivity. If :math:`\mathrm{t}_\mathrm{samp}`
is the sampling time of the observation, the single-pulse sensitivity
:math:`\mathrm{S}_\mathrm{sps}` can be written in terms of the continuum
sensitivity :math:`\mathrm{S}_\mathrm{cont}` as

.. math::
    \mathrm{S}_\mathrm{sps} = \frac{\beta}{\epsilon_\mathrm{sps}} \mathrm{S}_\mathrm{cont}(\mathrm{t}_\mathrm{obs}=\mathrm{t}_\mathrm{samp}) \sqrt{\frac{\mathrm{N}_\mathrm{ant}-1}{\mathrm{N}_\mathrm{ant}}} \sqrt{\frac{\mathrm{t}_\mathrm{samp}}{\mathrm{W}}}.

Dynamic range warning
=====================

In addition to calculating the theoretical sensitivity,
the LOW sensitivity calculator backend warns if it detects
a bright (>10 Jy) off-axis source within the full width at
half maximum (FWHM) of the LOW station beam calculated at
the centre of the specified band. The backend uses a bright
source catalogue derived from the GaLactic and Extragalactic
All-sky Murchison Widefield Array (GLEAM) survey
(`Hurley-Walker et al. (2017) <https://ui.adsabs.harvard.edu/link_gateway/2017MNRAS.464.1146H/doi:10.1093/mnras/stw2337>`_).

