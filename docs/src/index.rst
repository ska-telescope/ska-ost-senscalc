============
Introduction
============

The Sensitivity Calculator provides functionality for SKA scientists and engineers to
estimate sensitivities and integration times for observations on both SKA LOW and MID.

This document describes the theoretical background and details on the design and implementation
of the Calculator.

For more details on developing and deploying the code, see the `README <https://gitlab.com/ska-telescope/ost/ska-ost-senscalc/-/blob/master/README.md>`_


.. Hidden toctree to manage the sidebar navigation.

.. Overview section
.. toctree::
   :maxdepth: 1
   :caption: OVERVIEW
   :hidden:

   motivation

.. MID section
.. toctree::
   :maxdepth: 1
   :caption: MID
   :hidden:

   mid_background
   mid_implementation
   mid_rest_api

.. LOW section
.. toctree::
   :maxdepth: 1
   :caption: LOW
   :hidden:

   low_background
   low_implementation
   low_rest_api

.. Common section
.. toctree::
   :maxdepth: 1
   :caption: Common
   :hidden:

   weighting
   rascil
   spectropolarimetry

.. API section
.. toctree::
   :maxdepth: 1
   :caption: Public API Documentation
   :hidden:

   package/low_api
   package/low_validation
   package/low_model
   package/low_service
   package/low_calculator
   package/low_lookup
   package/mid_api
   package/mid_validation
   package/mid_service
   package/mid_calculator
   package/mid_sefd
   package/common_beam
   package/common_pss
   package/common_model
   package/subarray
   package/utilities
   package/mid_utilities

.. toctree::
   :maxdepth: 1
   :caption: Releases

   CHANGELOG.md