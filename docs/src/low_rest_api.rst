.. _low_rest_api:

*************
LOW REST API
*************

These docs are generated from the OpenAPI specification for the application. There is an issue with the RTD plugin
which means the response types are not rendered. To see the API response bodies for the status codes listed below
see the Swagger UI: https://k8s.stfc.skao.int/integration-ska-ost-senscalc-ui/senscalc/api/v<MAJOR_VERSION>/low/ui/

.. openapi:: ../../src/ska_ost_senscalc/static/openapi-low.yaml