.. _rascil:

===================
RASCIL Simulations
===================

The sensitivity of a radio image depends on the weighting scheme used when gridding the u,v data.
The sensitivity calculator attempts to model this using simulations carried out using the Radio Astronomy Simulation,
Calibration and Imaging Library (RASCIL). This is done by simulating the u,v coverage of an observation, applying the
weighting (uniform or Briggs/robust, including tapering) and then calculating the increase in sensitivity relative to natural weighting.
RASCIL also produces an image of the synthesized beam from which the major and minor axes of the main lobe can be measured.

The details of the simulations are as follows. They are carried out at a single frequency for Mid (1.4 GHz) and
Low (200 MHz) - the u,v coverage is essentially independent of frequency providing that an appropriate cellsize is used.
Simulations are carried out separately for a list of different subarrays in both 'line' (single channel) and 'continuum' (30 channels
covering a 30% bandwidth) mode. Multiple declinations are simulated ranging from -90 to one close to the lowest allowed elevation,
in 20 degree steps. Briggs weighting uses values of robust ranging from -2 to 2 in integer steps and tapering increases
by factors of 4 from an initial value of 0.25" (Mid) - tapering is not yet offered for Low.

Certain parameters are common to all simulations, including the observing time (1 hour), the correlator averaging
time (10s for 'line' and 60s for 'continuum'), the beam oversampling factor (8) and the size of the image (1024 x 1024 pixels).

The simulations are collected together in a look-up table which is used by the calculator to find the appropriate
weighting correction factor (increase in the rms noise relative to natural weighting) and synthesized beam-size, with
the latter being scaled to the frequency entered by the user. Values corresponding to the source declination are found
using interpolation. The weighting correction factor and beam-size are reported by the calculator and used to calculate
the final sensitivity (including the surface-brightness sensitivity in kelvins) or integration time.